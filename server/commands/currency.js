const admin = require('../lib/firebase-admin')
const { subtractViewerPoints, addViewerPoints } = require('../lib/points-operations')
const cleanUsername = require('../lib/clean-username')

function formatCurrency(coins) {
  return `${coins} ${coins === 1 ? 'coin' : 'coins'}`
}

function isInteger(x) {
  return (typeof x === 'number') && (x % 1 === 0)
}

function take(say, username, parts, channelId) {
  const target = cleanUsername(parts[1])
  const points = +parts[2]
  if (target && points && isInteger(points)) {
    if (points < 1) {
      say(`${username} give a number higher than zero`)
      return
    }
    subtractViewerPoints(channelId, target, points)
      .then(() => say(`${formatCurrency(points)} removed from ${target}`))
      .catch(() => say(`${target} has less than ${formatCurrency(points)}`))
  } else {
    say(`${username} Usage: !subtract [target] [amount]`)
  }
}

function give(say, username, parts, channelId) {
  const target = cleanUsername(parts[1])
  const points = +parts[2]
  if (target && points && isInteger(points)) {
    if (points < 1) {
      say(`${username} you must give at least 1 coin`)
      return
    }
    addViewerPoints(channelId, target, points)
      .then(() => say(`${formatCurrency(points)} given to ${target}`))
      .catch(e => console.log(e))
  } else {
    say(`${username} Usage: !give [target] [amount]`)
  }
}

function tip(say, username, parts, channelId) {
  const target = cleanUsername(parts[1])

  if (target.toLowerCase() === username.toLowerCase()) {
    say('You can\'t tip yourself!')
    return
  }

  const points = +parts[2]
  if (target && points && isInteger(points)) {
    if (points < 1) {
      say(`${username} you must tip at least 1 coin`)
      return
    }
    admin.database().ref(`channels/${channelId}/viewers/${target.toLowerCase()}/lastSeen`).once('value')
      .then((snapshot) => {
        const lastSeen = snapshot.val()
        if (lastSeen) {
          subtractViewerPoints(channelId, username, points).then(() => {
            addViewerPoints(channelId, target, points)
              .then(() => say(`${username} has given ${formatCurrency(points)} to ${target}`))
              .catch(e => console.log(e))
          }).catch(() => {
            say(`${username} You have less than ${formatCurrency(points)}!`)
          })
        } else {
          say(`${username} Hmm i haven't seen "${target}" around. Did you enter the right username?`)
        }
      })
  } else {
    say(`${username} Usage: !tip [target] [amount of coins]`)
  }
}

function balance(say, username, parts, channelId) {
  const target = cleanUsername(parts[1] || username)
  admin.database().ref(`channels/${channelId}/viewers/${target.toLowerCase()}/points`).once('value')
    .then((snapshot) => {
      const points = snapshot.val() || 0
      say(`${target} has ${formatCurrency(points)}`)
    })
}

module.exports = {
  give,
  take,
  tip,
  balance,
}
