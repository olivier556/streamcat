const admin = require('../lib/firebase-admin')

function commandAdd(say, parts, channelId) {
  const trigger = parts[2]
  const response = parts.slice(3).join(' ')
  if (!trigger || !response) {
    say('Syntax: !command add [command] [response]')
    return
  }
  const command = {
    trigger: trigger[0] === '!' ? trigger : `!${trigger}`,
    response,
    enabled: true,
    priv: 'EVERYONE',
  }
  admin.database().ref(`/userSettings/${channelId}/custom/${command.trigger}`).set(command)
  say(`Command set: ${command.trigger} -> ${command.response}`)
}

function commandDelete(say, parts, channelId) {
  if (!parts[2]) {
    say('Syntax: !command remove [command]')
    return
  }
  const trigger = parts[2][0] === '!' ? parts[2] : `!${parts[2]}`
  admin.database().ref(`/userSettings/${channelId}/custom/${trigger}`).remove((error) => {
    if (error) {
      say('Uh oh something bad happened. Try again later.')
    } else {
      say(`Command "${trigger}" deleted`)
    }
  })
}

function commandDisable(say, parts, channelId, enable = false) {
  if (!parts[2]) {
    say(`Syntax: !command ${enable ? 'enable' : 'disable'} [command]`)
    return
  }
  const trigger = parts[2][0] === '!' ? parts[2] : `!${parts[2]}`
  const ref = admin.database().ref(`/userSettings/${channelId}/custom/${trigger}`)

  ref.once('value', (snapshot) => {
    const value = snapshot.val()
    if (!value) {
      say(`Command "${trigger}" does not exist`)
      return
    }
    ref.set({ ...value, enabled: enable })
    say(`Command "${trigger}" has been ${enable ? 'enabled' : 'disabled'}`)
  })
}

function commandEnable(say, parts, channelId) {
  commandDisable(say, parts, channelId, true)
}

module.exports = {
  commandAdd,
  commandDelete,
  commandDisable,
  commandEnable,
}
