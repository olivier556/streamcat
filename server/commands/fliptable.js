function fliptable(say) {
  const flipped = [
    '(._.) ~ ︵ ┻━┻',
    '‎(ﾉಥ益ಥ）ﾉ﻿ ┻━┻',
    '(ノ ゜Д゜)ノ ︵ ┻━┻',
  ]
  const randomFlip = flipped[Math.floor(Math.random() * flipped.length)]
  say(randomFlip)
}

module.exports = fliptable
