const fetch = require('node-fetch')
const config = require('../../src/lib/config')
const moment = require('moment')

function followage(say, userId, channelId, username, prettyChannelName) {
  if (username.toLowerCase() === prettyChannelName.toLowerCase()) {
    say('The instant you created this channel')
    return
  }
  fetch(`https://api.twitch.tv/helix/users/follows?to_id=${channelId}&from_id=${userId}`, {
    method: 'GET',
    headers: {
      'Client-ID': config.twitchClientId,
    },
  })
    .then(res => res.json())
    .then((json) => {
      if (json.total === 0) {
        say(`${username} is not following ${prettyChannelName}`)
      } else {
        const date = json.data[0].followed_at
        const fromNow = moment(date).fromNow(true)
        say(`${username} has been following ${prettyChannelName} for ${fromNow}`)
      }
    })
}

module.exports = followage
