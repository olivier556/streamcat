const config = require('../../src/lib/config')
const admin = require('../lib/firebase-admin')
const { isUserFollowing, isUserSubscribed } = require('../lib/twitch-api')
const { subtractViewerPoints } = require('../lib/points-operations')

// Commands
const {
  give: giveCommand,
  take: takeCommand,
  tip: tipCommand,
  balance: balanceCommand,
} = require('./currency')
const {
  requestSong: requestSongCommand,
  currentSong: currentSongCommand,
  nextSong: nextSongCommand,
  skip: skipSongCommand,
  skipPay: skippaySongCommand,
} = require('./music')
const {
  commandAdd: commandAddCommand,
  commandDelete: commandDeleteCommand,
  commandEnable: commandEnableCommand,
  commandDisable: commandDisableCommand,
} = require('./custom')
const speedrunCommand = require('./speedrun')
const fliptableCommand = require('./fliptable')
const uptimeCommand = require('./uptime')
const eightballCommand = require('./8ball')
const wolframCommand = require('./wolfram')
const seenCommand = require('./seen')
const followageCommand = require('./followage')
const watchedCommand = require('./watched')

let userSettings = {}

admin.database().ref('/userSettings/').on('value', (snapshot) => {
  userSettings = snapshot.val() || {}
})

function* onMessage(
  client, channelId, fromUserId, username, prettyChannelName,
  userstate, message, channel, isMod,
) {
  function* isAllowed(minRank) {
    // everyone allowed
    if (minRank === 'EVERYONE') {
      return true
    }

    // is broadcaster
    if (prettyChannelName.toLowerCase() === username.toLowerCase()) {
      return true
    }

    // broadcasters only
    if (minRank === 'BROADCASTER') {
      return false
    }

    // only mods allowed
    if (minRank === 'MOD') {
      if (isMod) {
        return true
      }
      return false
    }

    // mods are allowed to do follower/sub commands
    if (isMod) {
      return true
    }

    // followers
    if (minRank === 'FOLLOWER') {
      const isFollower = yield isUserFollowing(channelId, fromUserId)
      return isFollower
    }
    // subs
    if (minRank === 'SUBSCRIBER') {
      const isSub = yield isUserSubscribed(channelId, fromUserId)
      return isSub
    }

    console.log('THIS SHOULD NEVER HAPPEN')
    return false
  }

  const parts = message.split(/\s+/)
  const command = parts[0]

  function getConfig(key, key2) {
    const settings = userSettings[channelId] && userSettings[channelId][key]
    if (!settings) {
      return false
    }

    if (key2 === 'maxSongLengthMinutes') {
      return (settings.maxSongLengthMinutes || 5) * 60
    }
    if (key2 === 'voteCountToSkip') {
      return settings.voteCountToSkip || 3
    }
    if (key2 === 'coinsEarnedPerMinute') {
      return settings.coinsEarnedPerMinute || 1
    }

    console.log('UH OH')
    return false
  }

  function* isCommandEnabled(key) {
    const settings = userSettings[channelId] && userSettings[channelId][key]
    if (!settings) {
      return false
    }

    const { enabled, minRank, price } = settings
    if (!enabled) {
      return false
    }

    const allowed = yield isAllowed(minRank)

    if (!allowed) {
      let notAllowedMessage
      if (minRank === 'BROADCASTER') {
        notAllowedMessage = 'Only the broadcaster can use this command'
      }
      if (minRank === 'MOD') {
        notAllowedMessage = 'This command is for mods only'
      }
      if (minRank === 'FOLLOWER') {
        notAllowedMessage = `Follow ${prettyChannelName} to unlock this command`
      }
      if (minRank === 'SUBSCRIBER') {
        notAllowedMessage = `Subscribe to ${prettyChannelName} to unlock this command`
      }
      client.say(channel, `${username} ${notAllowedMessage}`)
      return false
    }

    if (price > 0) {
      try {
        yield subtractViewerPoints(channelId, username, price)
        return true
      } catch (e) {
        client.say(channel, `${username} you do not have enough coins to use this command. "${command}" costs ${price} coins per use.`)
        return false
      }
    } else {
      return true
    }
  }

  if (userSettings[channelId]
    && userSettings[channelId].custom
    && userSettings[channelId].custom[command]) {
    if (userSettings[channelId].custom[command].enabled) {
      client.say(channel, userSettings[channelId].custom[command].response)
    }
  }

  // CUSTOM COMMANDS
  if (command === '!command') {
    if (parts[1] === 'add' && (yield isCommandEnabled('commandadd'))) {
      commandAddCommand(msg => client.say(channel, msg), parts, channelId)
    } else if (parts[1] === 'edit' && (yield isCommandEnabled('commandedit'))) {
      commandAddCommand(msg => client.say(channel, msg), parts, channelId)
    } else if (parts[1] === 'delete' && (yield isCommandEnabled('commanddelete'))) {
      commandDeleteCommand(msg => client.say(channel, msg), parts, channelId)
    } else if (parts[1] === 'enable' && (yield isCommandEnabled('commandenable'))) {
      commandEnableCommand(msg => client.say(channel, msg), parts, channelId)
    } else if (parts[1] === 'disable' && (yield isCommandEnabled('commanddisable'))) {
      commandDisableCommand(msg => client.say(channel, msg), parts, channelId)
    } else {
      // client.say(channel, 'Usage: !command [add OR edit OR delete OR enable OR disable]')
    }
    return
  }

  // INFORMATIONAL
  if (command === '!commands' && (yield isCommandEnabled('commands'))) {
    client.say(channel, `mokeybot commands for this channel: ${config.clientUrl}/commands/${prettyChannelName}`)
  }
  if (command === '!speedrun' && (yield isCommandEnabled('speedrun'))) {
    speedrunCommand(msg => client.say(channel, msg), username, parts)
  }
  if (command === '!seen' && (yield isCommandEnabled('seen'))) {
    seenCommand(msg => client.say(channel, msg), parts, username, channelId)
  }
  if (command === '!uptime' && (yield isCommandEnabled('uptime'))) {
    uptimeCommand(msg => client.say(channel, msg), prettyChannelName, channelId)
  }
  if (command === '!followage' && (yield isCommandEnabled('followage'))) {
    followageCommand(
      msg => client.say(channel, msg), fromUserId, channelId,
      username, prettyChannelName,
    )
  }
  if (command === '!watched' && (yield isCommandEnabled('watched'))) {
    watchedCommand(msg => client.say(channel, msg), parts, username, channelId)
  }

  // MUSIC
  if (command === '!playlist' && (yield isCommandEnabled('playlist'))) {
    client.say(channel, `Playlist: ${config.clientUrl}/playlist/${prettyChannelName}`)
  }
  if (command === '!sr' && (yield isCommandEnabled('sr'))) {
    requestSongCommand(msg => client.say(channel, msg), parts, username, fromUserId, channelId, getConfig('music', 'maxSongLengthMinutes'))
  }
  if (command === '!currentsong' && (yield isCommandEnabled('currentsong'))) {
    currentSongCommand(msg => client.say(channel, msg), channelId)
  }
  if (command === '!nextsong' && (yield isCommandEnabled('nextsong'))) {
    nextSongCommand(msg => client.say(channel, msg), channelId)
  }

  if (command === '!skip' && (yield isCommandEnabled('skip'))) {
    skipSongCommand(msg => client.say(channel, msg), channelId, username, getConfig('music', 'voteCountToSkip'))
  }
  if (command === '!skippay' && (yield isCommandEnabled('skippay'))) {
    skippaySongCommand(msg => client.say(channel, msg), channelId)
  }

  // FUN
  if (command === '!8ball' && (yield isCommandEnabled('8ball'))) {
    eightballCommand(msg => client.say(channel, msg))
  }
  if (command === '!fliptable' && (yield isCommandEnabled('fliptable'))) {
    fliptableCommand(msg => client.say(channel, msg))
  }
  if (command === '!wolfram' && (yield isCommandEnabled('wolfram'))) {
    wolframCommand(msg => client.say(channel, msg), parts)
  }

  // CURRENCY
  if (command === '!leaderboard' && (yield isCommandEnabled('leaderboard'))) {
    client.say(channel, `${config.clientUrl}/leaderboard/${prettyChannelName}`)
  }
  if (command === '!give' && (yield isCommandEnabled('give'))) {
    giveCommand(msg => client.say(channel, msg), username, parts, channelId)
  }
  if (command === '!tip' && (yield isCommandEnabled('tip'))) {
    tipCommand(msg => client.say(channel, msg), username, parts, channelId)
  }
  if (command === '!coins' && (yield isCommandEnabled('coins'))) {
    balanceCommand(msg => client.say(channel, msg), username, parts, channelId)
  }
  if (command === '!subtract' && (yield isCommandEnabled('subtract'))) {
    takeCommand(msg => client.say(channel, msg), username, parts, channelId)
  }
}

module.exports = onMessage
