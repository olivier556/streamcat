const fetch = require('node-fetch')
const queryString = require('query-string')
const moment = require('moment')
const isUrl = require('is-url')

const admin = require('../lib/firebase-admin')
const config = require('../../src/lib/config')

const ytkey = config.ytKey
let playlists = {}

admin.database().ref('/playlist/').on('value', (snapshot) => {
  playlists = snapshot.val() || {}
})

function prettifyPlaylist(rawPlaylist) {
  const playlist = Object
    .keys(rawPlaylist)
    .map(key => ({ ...rawPlaylist[key], id: key }))
    .sort((a, b) => a.timetamp - b.timestamp)

  return playlist
}

function getCurrentSong(channelId) {
  return prettifyPlaylist(playlists[channelId] || {})[0]
}

function getNextSong(channelId) {
  return prettifyPlaylist(playlists[channelId] || {})[1]
}

function skip(say, channelId, username, VOTES_TO_SKIP) {
  const song = getCurrentSong(channelId)

  if (!song) {
    say('There is no song to skip')
  } else {
    if (song.votesToSkip && song.votesToSkip[username]) {
      say(`${username} You've already voted to skip this song`)
      return
    }
    const currentVoteCount = song.votesToSkip
      ? (Object.keys(song.votesToSkip).length + 1)
      : 1
    if (currentVoteCount >= VOTES_TO_SKIP) {
      admin.database().ref(`playlist/${channelId}/${song.id}`).remove((error) => {
        if (error) {
          say('Something weird happened... Try that again')
        } else {
          say(`"${song.title}" has been skipped`)
        }
      })
    } else {
      const votesLeft = VOTES_TO_SKIP - currentVoteCount
      say(`${votesLeft} ${votesLeft === 1 ? 'vote' : 'votes'} left to skip "${song.title}"`)
      admin.database()
        .ref(`playlist/${channelId}/${song.id}`)
        .transaction(previousSong => ({
          ...previousSong,
          votesToSkip: {
            ...previousSong.votesToSkip,
            [username]: true,
          },
        }))
    }
  }
}

function skipPay(say, channelId) {
  const song = getCurrentSong(channelId)

  if (!song) {
    say('There is no song to skip')
  } else {
    admin.database().ref(`playlist/${channelId}/${song.id}`)
      .remove((error) => {
        if (error) {
          say('hmmm something weird happened. Try again in a bit.')
        } else {
          say('the current song has been skipped')
        }
      })
  }
}

function currentSong(say, channelId) {
  const song = getCurrentSong(channelId)

  if (song) {
    say(`"${song.title}" requested by ${song.requestedByUsername} is currently playing`)
  } else {
    say('No songs on playlist! Use "!sr [song name]" to add songs')
  }
}

function addSong(say, songId, username, fromUserId, channelId, MAX_DURATION_SECONDS) {
  const ytExtraInfoParams = {
    id: songId,
    part: 'contentDetails,snippet',
    key: ytkey,
  }
  return fetch(`https://www.googleapis.com/youtube/v3/videos?${queryString.stringify(ytExtraInfoParams)}`)
    .then(res => res.json())
    .then((json) => {
      const item = json.items[0]
      const songTitle = item.snippet.title
      const durationUnformatted = item.contentDetails.duration
      const durationInSeconds = moment.duration(durationUnformatted).asSeconds()
      if (durationInSeconds > MAX_DURATION_SECONDS) {
        say(`"${songTitle}" is too long! (max video duration is ${MAX_DURATION_SECONDS / 60} minutes)`)
        return
      }
      const toInsert = {
        timestamp: Date.now(),
        youtubeId: songId,
        durationInSeconds,
        title: songTitle,
        requestedByUsername: username,
        requestedByUserId: fromUserId,
      }
      const playlistRef = admin.database().ref(`playlist/${channelId}`).push()
      playlistRef.set(toInsert)
      say(`"${songTitle}" is added to the playlist. (${durationInSeconds}s)`)
    })
    .catch((e) => {
      console.log(e)
      say('Hmm something weird happened')
    })
}

function nextSong(say, channelId) {
  const song = getNextSong(channelId)

  if (song) {
    say(`"${song.title}" requested by ${song.requestedByUsername} will play next`)
  } else {
    say('There is no next song! Use "!sr [song name]" to add songs')
  }
}
function extractId(str) {
  const ytIdRegExp = new RegExp('[-_A-Za-z0-9]{10}[AEIMQUYcgkosw048]')

  if (!isUrl(str) && str.length !== 11) {
    return false
  }

  const match = str.match(ytIdRegExp)
  if (match && match[0]) {
    return match[0]
  }
  return false
}
function requestSong(say, parts, username, fromUserId, channelId, MAX_DURATION_SECONDS) {
  let toSearch = parts.slice(1).join(' ')
  const extractedId = extractId(toSearch)
  if (extractedId) {
    toSearch = extractedId
  }

  if (!toSearch) {
    say('Youtube link or song name is missing. Example: !sr congratulations')
  } else {
    if (extractedId) {
      addSong(say, extractedId, username, fromUserId, channelId, MAX_DURATION_SECONDS)
      return
    }
    const ytSearchParams = {
      q: toSearch,
      maxResults: 1,
      key: ytkey,
      type: 'video',
      videoEmbeddable: true,
      videoDuration: 'any',
      part: 'snippet',
    }
    fetch(`https://www.googleapis.com/youtube/v3/search?${queryString.stringify(ytSearchParams)}`)
      .then(res => res.json())
      .then((json) => {
        const item = json.items[0]
        if (!item) {
          say(`${username}, Hmm couldn't find that song`)
        } else {
          const songId = item.id.videoId
          addSong(say, songId, username, fromUserId, channelId, MAX_DURATION_SECONDS)
        }
      })
  }
}

module.exports = {
  requestSong,
  currentSong,
  nextSong,
  skip,
  skipPay,
}
