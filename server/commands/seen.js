const moment = require('moment')
const admin = require('../lib/firebase-admin')
const cleanUsername = require('../lib/clean-username')

function seen(say, parts, username, channelId) {
  const target = cleanUsername(parts[1])
  if (!target) {
    say(`${username} please add a target. i.e. !seen mokeybot`)
  } else {
    admin.database().ref(`channels/${channelId}/viewers/${target.toLowerCase()}/lastSeen`).once('value')
      .then((snapshot) => {
        const lastSeen = snapshot.val()
        if (lastSeen) {
          const lastSeenFromNow = moment(lastSeen).fromNow()
          say(`I last saw "${target}" ${lastSeenFromNow}`)
        } else {
          say(`Hmm i haven't seen "${target}" around`)
        }
      })
  }
}

module.exports = seen
