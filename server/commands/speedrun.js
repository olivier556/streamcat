const fetch = require('node-fetch')
const moment = require('moment')

const speedunSearchApiUrl = 'https://www.speedrun.com/ajax_search.php'
const speedrunApiUrl = 'https://www.speedrun.com/api/v1'

function extractText(str) {
  let ret = ''
  if (/"/.test(str)) {
    ret = str.match(/"(.*?)"/)[1]
  } else {
    ret = str
  }
  return ret
}

function speedrun(say, username, parts) {
  function getWr(game, categoryId, gameName, categoryName) {
    fetch(`${speedrunApiUrl}/games/${game}/records`)
      .then(res => res.json())
      .then((json3) => {
        const categories = json3.data
        const runs = categories.filter(({ category }) => {
          return category === categoryId
        })[0].runs
        const userId = runs[0].run.players[0].id
        const time = moment.duration(runs[0].run.times.primary).asMilliseconds()
        fetch(`${speedrunApiUrl}/users/${userId}`)
          .then(res => res.json())
          .then((json4) => {
            const formattedTime = moment.utc(time).format('HH:mm:ss')
            const runAuthor = json4.data.names.international
            say(`wr for ${gameName} -> ${categoryName} is ${formattedTime} by ${runAuthor}`)
          })
      })
  }
  const game = extractText(parts.slice(1).join(' '))
  if (!game) {
    say(`${username} please add a game: i.e. !speedrun "Getting Over It"`)
  } else {
    fetch(`${speedunSearchApiUrl}?term=${game}`)
      .then(res => res.json())
      .then((json) => {
        const gameFromSearch = json.filter(x => x.category === 'Games')[0]
        if (!gameFromSearch) {
          say(`${username} could not find that game`)
        } else {
          fetch(`${speedrunApiUrl}/games/${gameFromSearch.url}/categories`)
            .then(res => res.json())
            .then(json2 => {
              if (json2.data.length > 1) {
                if (parts.slice(1).join(' ').indexOf('"') === -1) {
                  say(`${username} Specify a category: ${json2.data.map(x => x.name).join(', ')}`)
                } else {
                  const categoryName = parts.slice(1).join(' ').split('"').pop().toLowerCase().trim()
                  const category = json2.data.filter(x => x.name.toLowerCase() === categoryName)[0]

                  if (category) {
                    const categoryId = category.id
                    const categoryName = category.name
                    getWr(gameFromSearch.url, categoryId, gameFromSearch.label, categoryName)
                  } else {
                    say(`${username} Specify a valid category: ${json2.data.map(x => x.name).join(', ')}`)
                  }
                }
              } else {
                const categoryId = json2.data[0].id
                const categoryName = json2.data[0].name
                getWr(gameFromSearch.url, categoryId, gameFromSearch.label, categoryName)
              }
            })
        }
      })
  }
}

module.exports = speedrun
