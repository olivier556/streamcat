const moment = require('moment')
const fetch = require('node-fetch')
const config = require('../../src/lib/config')

function uptime(say, prettyChannelName, channelId) {
  fetch(`https://api.twitch.tv/helix/streams/?user_id=${channelId}`, {
    method: 'GET',
    headers: {
      'Client-ID': config.twitchClientId,
    },
  })
    .then(res => res.json())
    .then((json) => {
      if (json.data.length === 0) {
        // user is offline
        say(`${prettyChannelName} is offline!`)
      } else {
        // user is online
        const now = moment(new Date())
        const startedAt = json.data[0].started_at
        const uptimeMoment = moment(startedAt)
        const hours = now.diff(uptimeMoment, 'hours')
        uptimeMoment.add(hours, 'hours')
        const minutes = now.diff(uptimeMoment, 'minutes')
        uptimeMoment.add(minutes, 'minutes')
        const seconds = now.diff(uptimeMoment, 'seconds')

        let formatted
        if (hours && minutes && seconds) {
          formatted = `${hours}h ${minutes}m ${seconds}s`
        } else if (minutes && seconds) {
          formatted = `${minutes}m ${seconds}s`
        } else if (seconds) {
          formatted = `${seconds}s`
        } else {
          console.log('WTF')
        }
        say(`${prettyChannelName} has been live for ${formatted}`)
      }
    })
}

module.exports = uptime
