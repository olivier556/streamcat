const admin = require('firebase-admin')
const cleanUsername = require('../lib/clean-username')

function watched(say, parts, username, channelId) {
  const target = cleanUsername(parts[1] || username)
  admin.database().ref(`channels/${channelId}/viewers/${target.toLowerCase()}/minutesWatched`).once('value')
    .then((snapshot) => {
      const minutesWatched = snapshot.val()
      const hoursWatched = (minutesWatched / 60).toFixed(1)

      if (minutesWatched) {
        say(`${target} has ${hoursWatched} hours of watchtime`)
      } else {
        say(`Hmm i haven't seen "${target}" around`)
      }
    })
}

module.exports = watched
