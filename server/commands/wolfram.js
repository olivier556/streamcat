const wolfram = require('wolfram').createClient('499Q7P-4WE35YJY5P')

function wolframCommand(say, parts) {
  const toSearch = parts.slice(1).join(' ')
  if (!toSearch) return
  wolfram.query(toSearch, (err, result) => {
    if (err) {
      console.log('wolfram error')
    } else {
      let output = 'Try again.'
      if (result && result[1]) {
        output = err
          || result[1].subpods[0].value
          || result[1].subpods[0].image
          || JSON.stringify(result[1].subpods[0])
          || 'Try again.'
      }
      say(output.trim().replace('\n', ' '))
    }
  });
}

module.exports = wolframCommand
