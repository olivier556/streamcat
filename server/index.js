const express = require('express')
const request = require('request')
const queryString = require('query-string')
const cors = require('cors')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const https = require('https')
const http = require('http')
const fs = require('fs')
const ejs = require('ejs')
const config = require('../src/lib/config')
const admin = require('./lib/firebase-admin')
const getBotToken = require('./lib/get-bot-token')

require('./lib/twitch-events')
require('./lib/bitcoin-events')
require('./lib/twitch-viewers')


const isProduction = process.env.NODE_ENV === 'production'
console.log('Starting with', isProduction ? 'PRODUCTION' : 'DEVELOPMENT', 'config')

const ejsTemplates = {
  bitcoinDonation: fs.readFileSync('./widgets/bitcoin-donation.html', 'utf-8'),
  simpleNotifications: fs.readFileSync('./widgets/simple-notifications.html', 'utf-8')
}

const app = express()
if (isProduction) {
  app.use(helmet({
    frameguard: false
  }))
}

app.use(bodyParser.json())
app.use(cors())
app.use(express.static('./widgets/public'))

app.get('/bitcoin-donation', (req, res) => {
  const html = ejs.render(ejsTemplates.bitcoinDonation, {
    firebaseApiKey: config.firebaseApiKey,
    firebaseDatabaseURL: config.firebaseDatabaseURL,
    firebaseProjectId: config.firebaseProjectId
  })
  res.send(html)
})
app.get('/simple-notifications', (req, res) => {
  const html = ejs.render(ejsTemplates.simpleNotifications, {
    firebaseApiKey: config.firebaseApiKey,
    firebaseDatabaseURL: config.firebaseDatabaseURL,
    firebaseProjectId: config.firebaseProjectId
  })
  res.send(html)
})

app.get('/', (req, res) => {
  res.send('Hey my guy')
})

app.get('/follower-webhook', (req, res) => {
  const challenge = req.query['hub.challenge']
  if (challenge) {
    res.status(200).send(challenge)
  } else {
    console.log('Did not receive challenge')
  }
})

app.post('/follower-webhook', (req, res) => {
  res.sendStatus(200)
  const userId = req.body.data.to_id;
  const fromUserId = Number(req.body.data.from_id)

  getBotToken().then(token => {
    request({
      url: 'https://api.twitch.tv/helix/users?id=' + fromUserId,
      headers: {
        'Authorization': 'Bearer ' + token,
        'Client-ID': config.twitchClientId
      }
    }, function(error, response, body) {
      if (error) {
        console.log(error)
        return
      }
      let json

      try {
        json = JSON.parse(body).data[0]
      } catch(e) {
        console.log('error parsing this as json', body)
        return
      }

      if (!json) {
        console.log(json, error)
        return
      }

      const displayName = json.display_name
      const viewers = json.view_count
      const profileImageUrl = json.profile_image_url

      const toInsert = {
        timestamp: Date.parse(req.body.timestamp),
        fromUserId,
        username: displayName,
        viewers,
        profileImageUrl
      }

      admin.database().ref(`channels/${userId}/followers/${fromUserId}`).set(toInsert)
    })
  })
})

app.get('/login', function(req, res){
  const twitchCode = req.query.twitchCode

  console.log('login attempt', twitchCode)

  if (!twitchCode) {
    res.sendStatus(400)
    console.log('no twitch code')
    return
  }

  const params = {
    client_id: config.twitchClientId,
    client_secret: config.twitchClientSecret,
    code: twitchCode,
    grant_type: 'authorization_code',
    redirect_uri: config.twitchRedirectUri
  }

  request.post('https://api.twitch.tv/kraken/oauth2/token?' + queryString.stringify(params), function(error, response, body) {
    if (error) {
      res.sendStatus(400)
    }
    try {
      const token = JSON.parse(body)

      request({
        url: 'https://api.twitch.tv/helix/users',
        headers: {
          'Authorization': 'Bearer ' + token.access_token,
          'Client-ID': config.twitchClientId
        }
      }, function(error, response, body2) {
        if (error || body2.error) {
          res.sendStatus(400)
          return
        }

        const user = JSON.parse(body2).data[0]

        const toInsert = {
          username: user.login,
          token: token.access_token,
          refreshToken: token.refresh_token,
          scope: token.scope,
          expires: Date.now() + (token.expires_in * 1000),
          userId: user.id
        }
        admin.database().ref('usernameToIdMap/' + toInsert.username.toLowerCase()).set(user.id)
        admin.database().ref('restrictedAccess/twitchTokens/' + user.id).set(toInsert)

        admin.auth().createCustomToken(user.id)
          .then(function(customToken) {
            res.send({
              firebaseToken: customToken,
              username: user.login,
              userId: user.id
            })
          })
          .catch(function(error) {
            res.sendStatus(400)
          })
      })

    } catch(e) {
      res.sendStatus(400)
    }
  })
})

// function syncSubsAndFollowers(userId, token) {
//   console.log('SYNC!')
//   // GET 25 LATEST FOLLOWERS
//   fetch(`https://api.twitch.tv/helix/users/follows?first=25&to_id=${userId}`, {
//     method: 'GET',
//     headers: {
//       'Client-ID': config.twitchClientId
//     }
//   })
//     .then(res => res.json())
//     .then(json => {
//       console.log('FOLLLOWERS', json)
//     })
//
//   // GET 25 LATEST SUBSCRIBERS
//   fetch(`https://api.twitch.tv/kraken/channels/${userId}/subscriptions?limit=25`, {
//     method: 'GET',
//     headers: {
//       'Client-ID': config.twitchClientId,
//       Authorization: `OAuth ${token}`
//     }
//   })
//     .then(res => res.json())
//     .then(json => {
//       console.log('SUBS', json)
//     })
// }

const options = {
  key: fs.readFileSync('./keys/key.pem'),
  cert: fs.readFileSync('./keys/cert.pem'),
  passphrase: 'montyvstheworld'
};

if (isProduction) {
  http.createServer(app).listen(80)
  https.createServer(options, app).listen(443)
} else {
  app.listen(config.devServerPort)
}
