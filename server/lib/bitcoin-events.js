const WebSocket = require('ws')
const admin = require('./firebase-admin')
const fetch = require('node-fetch')

const url = 'wss://ws.blockchain.info/inv'
const ws = new WebSocket(url)

const watching = {}

ws.on('open', function open() {
  console.log('SOCKET IS OPEN')

  admin.database().ref('/userSettings').on('value', function(snapshot) {
    const val = snapshot.val()

    // No user settings yet
    if (!val) return

    const userAddressPair = Object.keys(val)
      .map(userId => ({userId, bitcoinAddress: val[userId].bitcoinAddress}))
      .filter(obj => !!obj.bitcoinAddress)

    userAddressPair.forEach(pair => {
      if (!watching[pair.userId]) {
        sub(pair)
      } else if (watching[pair.userId] !== pair.bitcoinAddress) {
        unsub(pair)
        sub(pair)
      }
    })

    console.log(userAddressPair)
    console.log(watching)
  })

  // ws.send(JSON.stringify({"op":"unconfirmed_sub"}))

  function sub({bitcoinAddress, userId}) {
    watching[userId] = bitcoinAddress
    ws.send(JSON.stringify({
      op: 'unconfirmed_sub',
      addr: bitcoinAddress
    }))
  }
  function unsub({bitcoinAddress, userId}) {
    delete watching[userId]
    ws.send(JSON.stringify({
      op: 'unconfirmed_unsub',
      addr: bitcoinAddress
    }))
  }
});

ws.on('message', function incoming(data) {
  console.log(data)
  const transaction = JSON.parse(data).x.out[0]
  const recipientBitcoinAddress = transaction.addr
  const total = transaction.value / 100000000
  let userId
  Object.keys(watching).forEach(id => {
    if (watching[id] === recipientBitcoinAddress) {
      userId = id
    }
  })

  if (!userId) {
    console.log('nah')
    return
  }
  fetch('https://blockchain.info/tobtc?currency=USD&value=' + transaction.value)
    .then(res => {
      return res.json()
    })
    .then(res => {
      const toInsert = {
        totalBtc: total,
        totalUsd: res.toFixed(2),
        timestamp: Date.now()
      }
      const btcDonationList = admin.database().ref(`channels/${userId}/bitcoinDonation`).push()
      btcDonationList.set(toInsert);
      console.log(total + ' BTC DONATION! ($' + res.toFixed(2) + ' USD)')
    })
});
