
function cleanUsername(username) {
  return username && username[0] === '@' ? username.slice(1) : username
}

module.exports = cleanUsername
