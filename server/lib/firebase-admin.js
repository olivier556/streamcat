const admin = require('firebase-admin')
const config = require('../../src/lib/config')

const serviceAccount = require('../keys/' + config.firebaseAdminKeyName + '.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: config.firebaseDatabaseURL
})

module.exports = admin
