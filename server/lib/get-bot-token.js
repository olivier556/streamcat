const fetch = require('node-fetch')
const queryString = require('query-string')
const admin = require('./firebase-admin')
const config = require('../../src/lib/config')

function getBotToken() {
  return new Promise((resolve, reject) => {
    admin.database().ref('/restrictedAccess/twitchTokens/' + config.botUserId).once('value', snapshot => {
      const val = snapshot.val()
      if (!val) {
        console.log('ERROR! bot token not found')
        return
      }

      const isExpired = val.expires < Date.now()
      console.log('Is token expired?', isExpired, val.expires, Date.now())
      if (isExpired) {
        console.log('Expired bot token... refreshing')
        const params = {
          grant_type: 'refresh_token',
          refresh_token: val.refreshToken,
          client_id: config.twitchClientId,
          client_secret: config.twitchClientSecret
        }
        fetch('https://api.twitch.tv/kraken/oauth2/token?' + queryString.stringify(params), {
          method: 'POST'
        })
          .then(body => body.json())
          .then(json => {
            console.log('NEW BOT TOKEN', JSON.stringify(json, true, 2))
            const toInsert = {
              ...val,
              token: json.access_token,
              refreshToken: json.refresh_token,
              scope: json.scope,
              expires: Date.now() + (json.expires_in * 1000),
            }
            admin.database().ref('restrictedAccess/twitchTokens/' + val.userId).set(toInsert)
            resolve(json.access_token)
          })
      } else {
        resolve(val.token)
      }
    });
  });
}

module.exports = getBotToken
