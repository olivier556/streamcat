const fetch = require('node-fetch')
const config = require('../../src/lib/config')

const memCache = {}

function* getGameById(gameId) {
  if (memCache[gameId]) {
    return memCache[gameId]
  }

  const res = yield fetch(`https://api.twitch.tv/helix/games?id=${gameId}`, {
    method: 'GET',
    headers: {
      'Client-ID': config.twitchClientId,
    },
  })

  const json = yield res.json()
  const rawGame = json.data[0]
  const game = {
    gameId,
    gameName: rawGame.name,
    gameThumbnailUrl: rawGame.box_art_url,
  }

  memCache[gameId] = game
  return game
}

module.exports = getGameById
