const admin = require('./firebase-admin')

function subtractViewerPoints(channelUserId, username, pointsToDelete) {
  return new Promise((resolve, reject) => {
    const ref = admin.database().ref(`channels/${channelUserId}/viewers/${username}/points`)
    let isGood = false
    ref.transaction(points => {
      if (pointsToDelete <= points) {
        isGood = true
        return points - pointsToDelete
      } else {
        isGood = false
        return points
      }
    }, function(error) {
      if (error || !isGood) {
        reject(error)
      } else {
        resolve()
      }
    })
  })
}

function addViewerPoints(channelUserId, username, pointsToAdd) {
  const ref = admin.database().ref(`channels/${channelUserId}/viewers/${username.toLowerCase()}/points`)
  return ref.transaction(points => points + pointsToAdd)
}

function setViewerPoints(channelUserId, viewerUserId, pointsToSet) {
  const ref = admin.database().ref(`channels/${channelUserId}/viewers/${username.toLowerCase()}/points`)
  ref.set(pointsToSet)
}

module.exports = {
  subtractViewerPoints,
  addViewerPoints,
  setViewerPoints
}
