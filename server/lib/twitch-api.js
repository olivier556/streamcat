const admin = require('./firebase-admin')
const config = require('../../src/lib/config')
const fetch = require('node-fetch')

function* isUserFollowing(channelId, userId) {
  const res = yield fetch(`https://api.twitch.tv/helix/users/follows?to_id=${channelId}&from_id=${userId}`, {
    method: 'GET',
    headers: {
      'Client-ID': config.twitchClientId,
    },
  })
  const json = yield res.json()

  if (json.total === 0) {
    // not following
    return false
  }
  // yes following
  return true
}

function* isUserSubscribed(channelId, userId) {
  const channelToken = (yield admin.database().ref(`restrictedAccess/twitchTokens/${channelId}/token`).once('value')).val()

  if (!channelToken) {
    return true
  }

  const res = yield fetch(`https://api.twitch.tv/kraken/channels/${channelId}/subscriptions/${userId}`, {
    method: 'GET',
    headers: {
      'Client-ID': config.twitchClientId,
      Authorization: `OAuth ${channelToken}`,
    },
  })
  const json = yield res.json()
  console.log('sub res', json)

  if (json.user) {
    return true
  }
  return false
}

module.exports = {
  isUserFollowing,
  isUserSubscribed,
}
