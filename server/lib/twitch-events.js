const tmi = require('tmi.js')
const admin = require('./firebase-admin')
const getBotToken = require('./get-bot-token')
const fetch = require('node-fetch')
const queryString = require('query-string')
const config = require('../../src/lib/config')
const commands = require('../commands/index')
const co = require('co')

const ref = admin.database().ref('/restrictedAccess/twitchTokens')

let newItems = false

ref.once('value')
  .then(snapshot => snapshot.val())
  .then((value) => {
    newItems = true
    const usernames = Object.keys(value).map(userId => value[userId].username)
    const userIds = Object.keys(value)

    // HARDCODING DRDISRESPECT
    // usernames.push('buckiize')
    // usernames.push('lumonen')
    // usernames.push('spyrunite')
    // usernames.push('montyvstheworld')
    // usernames.push('jlongopoker')
    // userIds.push(17337557)

    getBotToken()
      .then((token) => {
        startServiceAndJoinChannels(usernames, token)
        userIds.forEach((userId) => {
          webhookFollows(userId, token)
        })
      })
  })

ref.on('child_added', snapshot => {
  if (!newItems) {
    return
  }
  const value = snapshot.val()
  console.log('NEW USER', value)
  const username = value.username
  const userId = value.userId

  getBotToken()
    .then((token) => {
      joinChannel(username)
      webhookFollows(userId, token)
    })
})

function startServiceAndJoinChannels(usernames, token) {
  const options = {
    options: {
      debug: false
    },
    connection: {
      reconnect: true
    },
    identity: {
      username: config.botUsername,
      password: `oauth:${token}`
    },
    channels: usernames
  };

  client = new tmi.client(options)
  client.connect()

  client.on('join', (channel, username, self) => {
    if (self) {
      console.log(`JOINED ${channel}`)
    }
  })

  client.on('chat', (channel, userstate, message, self) => {
    if (!self) {
      const channelId = userstate['room-id']
      const fromUserId = userstate['user-id']
      const username = userstate.username
      const prettyChannelName = channel.slice(1)
      const isMod = userstate['user-type'] === 'mod' || username === channel.replace('#', '')
      co(commands(client, channelId, fromUserId, username, prettyChannelName, userstate, message, channel, isMod))
    }
  })

  client.on('cheer', (channel, userstate, message) => {
    const channelId = userstate['room-id']
    const fromUserId = userstate['user-id']
    const username = userstate.username
    const bits = userstate.bits

    const toInsert = {
      message,
      emotes: userstate.emotes,
      username,
      timestamp: Number(userstate['tmi-sent-ts']),
      bits: userstate.bits,
      fromUserId
    }

    admin.database().ref(`channels/${channelId}/cheers/${fromUserId}`).set(toInsert)
    console.log(`${bits} bits donated by ${username}`)
  })

  client.on('resub', (channel, username, months, message, userstate, methods) => {
    const channelId = userstate['room-id']
    const fromUserId = userstate['user-id']

    const toInsert = {
      message,
      emotes: userstate.emotes,
      username,
      timestamp: Number(userstate['tmi-sent-ts']),
      plan: methods.plan,
      months,
      fromUserId
    }
    admin.database().ref(`channels/${channelId}/subscribers/${fromUserId}`).set(toInsert);

    console.log(`resub by ${username} for ${months} months TIER ${methods.plan}`)
  })

  client.on('subscription', (channel, username, method, message, userstate) => {
    const channelId = userstate['room-id']
    const fromUserId = userstate['user-id']

    const toInsert = {
      message,
      emotes: userstate.emotes,
      username,
      timestamp: Number(userstate['tmi-sent-ts']),
      plan: method.plan,
      months: 1,
      fromUserId
    }
    admin.database().ref(`channels/${channelId}/subscribers/${fromUserId}`).set(toInsert);

    console.log(`sub by ${username} TIER ${method.plan}`)
  })
}

function joinChannel(str) {
  client.ws.send(`JOIN ${formatChannelStr(str)}`);
}

function formatChannelStr(str) {
	var channel = typeof str === "undefined" || str === null ? "" : str;
	return channel.charAt(0) === "#" ? channel.toLowerCase() : "#" + channel.toLowerCase();
}

function webhookFollows(userId, token) {
  const webhookSecret = 'simplddddddddd223';
  const params = {
    'hub.mode': 'subscribe',
    'hub.topic': `https://api.twitch.tv/helix/users/follows?to_id=${userId}`,
    'hub.callback': `${config.twitchWebhookUrl}/follower-webhook`,
    'hub.lease_seconds': 864000,
    'hub.secret': webhookSecret
  }

  fetch(`https://api.twitch.tv/helix/webhooks/hub?${queryString.stringify(params)}`, {
    method: 'POST',
    headers: {
      'Client-ID': config.twitchClientId
    }
  })
    .then(res => {
      if (res.status === 202) {
        console.log('SUCCESFULLY ATTACHED WEBHOOK')
      } else {
        console.log('ERROR SETTING WEBHOOK!')
      }
    })
    .catch(error => {
      console.log(error)
    })
}
