const admin = require('./firebase-admin')
const fetch = require('node-fetch')
const co = require('co')
const queryString = require('query-string')
const config = require('../../src/lib/config')
const getGameById = require('./get-game-by-id')

let userSettings = {}

admin.database().ref('/userSettings/').on('value', (snapshot) => {
  userSettings = snapshot.val() || {}
})

function coinsEarnedPerMinute(channelId) {
  return (
    userSettings[channelId]
    && userSettings[channelId].currency
    && userSettings[channelId].currency.coinsEarnedPerMinute
  ) || 1
}

function* getOnlineAccounts() {
  const accounts = yield getAccounts()
  const accountIds = accounts.map(account => account.userId)

  if (accountIds.length > 50) {
    console.log('MORE THAN 50 IDS! REQUEST WILL FAIL!')
  }

  const params = {
    user_id: accountIds
  }

  // this will only work with up to 50 IDs
  const res = yield fetch(`https://api.twitch.tv/helix/streams/?${queryString.stringify(params)}`, {
    method: 'GET',
    headers: {
      'Client-ID': config.twitchClientId
    }
  })
  const json = yield res.json()

  const onlineUserIds = []

  for (let i = 0; i < json.data.length; i += 1) {
    const data = json.data[i]
    const userId = data.user_id
    onlineUserIds.push(userId)
    const game = yield getGameById(data.game_id)
    const toInsert = {
      ...game,
      viewerCount: data.viewer_count,
      timestamp: Date.now(),
      title: data.title,
      type: data.type,
      thumbnailUrl: data.thumbnail_url,
    }
    yield admin.database().ref(`channels/${userId}/meta/${Date.parse(data.started_at)}`).push().set(toInsert)
  }

  const onlineAccounts = accounts.filter(account => onlineUserIds.indexOf(account.userId) !== -1)
  console.log(onlineAccounts.length, 'online users |', accountIds.length, 'total users')
  return onlineAccounts
}

function getAccounts() {
  return admin.database().ref('/restrictedAccess/twitchTokens').once('value')
    .then(snapshot => {
      const value = snapshot.val()
      const users = Object.keys(value).map(userId => ({
        username: value[userId].username,
        userId
      }))
      return users
    })
}

function* getViewers(username) {
  const res = yield fetch(`http://tmi.twitch.tv/group/user/${username}/chatters`)
  const json = yield res.json()
  const viewers = [
    ...json.chatters.moderators,
    ...json.chatters.global_mods,
    ...json.chatters.staff,
    ...json.chatters.viewers,
    ...json.chatters.admins
  ]
  return viewers
}

function* getViewersAllUsers() {
  const users = yield getOnlineAccounts()
  for (let i = 0; i < users.length; i++) {
    const user = users[i]
    const viewers = yield getViewers(user.username)
    console.log(user.username, viewers.length)
    yield updateViewerPoints(user.userId, viewers)
  }
}

function updateViewerPoints(userId, viewers) {
  const ref = admin.database().ref(`channels/${userId}/viewers`)
  return ref.transaction(currentData => {
    if (!currentData) {
      const obj = {}
      viewers.forEach(viewer => obj[viewer] = {
        points: coinsEarnedPerMinute(userId),
        lastSeen: Date.now(),
        minutesWatched: 1
      })
      return obj
    } else {
      const obj = currentData
      viewers.forEach(viewer => {
        obj[viewer] = obj[viewer] ? {
          lastSeen: Date.now(),
          points: obj[viewer].points + coinsEarnedPerMinute(userId),
          minutesWatched: obj[viewer].minutesWatched + 1
        } : {
          lastSeen: Date.now(),
          points: coinsEarnedPerMinute(userId),
          minutesWatched: 1
        }
      })
      return obj
    }
  })
}

setInterval(() => {
  console.log('TICK')
  co(getViewersAllUsers)
}, 60 * 1000)

// updateViewerPoints('125677609', [{username: 'dddd', userId: 'rrr34'}, {username: 'ddxxx', userId: '22222'}])
// subtractViewerPoints('125677609', 'rrr34', 1, valid => console.log(valid) )
// addViewerPoints('125677609', 'rrr34', 5, valid => console.log(valid) )
// setViewerPoints('125677609', 'rrr34', 10)
// co(getViewersAllUsers)
