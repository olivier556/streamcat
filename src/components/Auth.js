import React, { Component } from 'react';
import { Loader } from 'semantic-ui-react'

class Auth extends Component {
  render() {
    return <Loader className="Loader-text" active size='massive'>Loading</Loader>
  }
}

export default Auth
