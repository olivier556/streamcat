import React from 'react'
import './Cat.css'

export const Cat = () => (
  <div className="cat">
    <div className="ear ear--left" />
    <div className="ear ear--right" />
    <div className="face">
      <div className="eye eye--left">
        <div className="eye-pupil" />
      </div>
      <div className="eye eye--right">
        <div className="eye-pupil" />
      </div>
      <div className="muzzle" />
    </div>
  </div>
)

export default Cat
