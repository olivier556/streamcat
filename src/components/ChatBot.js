import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid } from 'react-flexbox-grid';

import './Chatbot.css';
import { updateUserSettings } from '../ducks/userSettings'
import defaultCommands from '../lib/default-commands'
import Command from './Command'
import ChatBotSettings from './ChatBotSettings'

class ChatBot extends Component {
  getCommandConfig(key) {
    if (!this.props.userSettings[key]) {
      return false
    } else {
      return this.props.userSettings[key]
    }
  }
  toggleCommand(key) {
    const newStateEnabled = !this.getCommandConfig(key).enabled
    const newState = {
      enabled: newStateEnabled
    }
    this.props.updateUserSettings({ [key]: newState })
  }
  toggleCustomCommand(key) {
    const command = this.props.userSettings.custom[key]
    this.props.updateUserSettings({
      custom: {
        ...this.props.userSettings.custom,
        [key]: {
          ...command,
          enabled: !command.enabled,
        },
      },
    })
  }
  updateSettings(key, nextSettings) {
    this.props.updateUserSettings({
      [key]: {
        ...this.getCommandConfig(key),
        ...nextSettings,
      },
    })
  }
  render() {
    if (!this.props.userSettings) {
      return null
    }
    const customCommands = this.props.userSettings.custom
      ? Object.keys(this.props.userSettings.custom).map(key => this.props.userSettings.custom[key])
      : []

    return (
      <Grid className="Widgets">
        {customCommands.length > 0 && (
          <div>
            <h1 className="Widgets-title">CUSTOM</h1>
            {customCommands.map(command => (
              <Command
                key={command.trigger}
                instructions={`Response -> ${command.response}`}
                isEnabled={command.enabled}
                command={command.trigger}
                toggle={() => this.toggleCustomCommand(command.trigger)}
                noSettings
              />
            ))}
          </div>
        )}
        {defaultCommands.map((category) => {
          const commands = category.commands.map((command) => {
            const config = this.getCommandConfig(command.key)
            if (!config) return null
            const { enabled, minRank, price } = config
            return (
              <Command
                key={command.key}
                wip={command.wip}
                instructions={command.instructions}
                isEnabled={enabled}
                minRank={minRank}
                price={price}
                toggle={() => this.toggleCommand(command.key)}
                updateSettings={nextSettings => this.updateSettings(command.key, nextSettings)}
                command={command.command}
              />
            )
          })
          const categorySettings = category.key && this.getCommandConfig(category.key)
          return (
            <div key={category.title}>
              <h1 className="Widgets-title">{category.title}</h1>
              {categorySettings && (
                <ChatBotSettings category={category} config={categorySettings} />
              )}
              {commands}
            </div>
          )
        })}
      </Grid>
    )
  }
}

export default connect(state => ({
  userSettings: state.userSettings,
}), dispatch => ({
  updateUserSettings: userSettings => dispatch(updateUserSettings(userSettings)),
}))(ChatBot);
