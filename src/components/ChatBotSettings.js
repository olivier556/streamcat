import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './ChatBotSettings.css'

class ChatBotSettings extends Component {
  constructor(props) {
    super(props)
    this.renderByKey = this.renderByKey.bind(this)
  }
  renderByKey(key) {
    const { config } = this.props
    const value = config[key]

    if (key === 'maxSongLengthMinutes') {
      return `Requested songs must be ${value} minutes or less.`
    } else if (key === 'voteCountToSkip') {
      return `${value} skips (!skip) required to skip a song.`
    } else if (key === 'maxSongsPerUser') {
      return `${value} songs per user max`
    } else if (key === 'coinsEarnedPerMinute') {
      return `Viewers earn ${value} coin per minute of watchtime`
    }
    console.log('WOT')
    return null
  }
  render() {
    const config = Object.keys(this.props.category.defaultConfig).map(configKey => (
      <div key={configKey}>
        <div className="ChatBotSettings-text">
          {this.renderByKey(configKey)}
        </div>
      </div>
    ))
    return (
      <div className="ChatBotSettings">
        {config}
      </div>
    )
  }
}

ChatBotSettings.propTypes = {
  category: PropTypes.object.isRequired,
  config: PropTypes.object.isRequired,
}

export default ChatBotSettings
