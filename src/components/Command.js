import React, { Component } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import { Button, Modal } from 'semantic-ui-react'
import { RANKS } from '../lib/default-commands'
import './Command.css'

class Command extends Component {
  constructor(props) {
    super(props)
    this.state = {
      draftMinRank: 'EVERYONE',
      draftPrice: 0,
      isModalOpen: false,
    }
    this.onChangePrice = this.onChangePrice.bind(this)
    this.onChangeMinRank = this.onChangeMinRank.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
    this.save = this.save.bind(this)
  }
  onChangePrice(e) {
    this.setState({
      draftPrice: Math.max(e.target.value, 0),
    })
  }
  onChangeMinRank(e) {
    this.setState({
      draftMinRank: e.target.value,
    })
  }
  save() {
    this.toggleModal()
    const { draftPrice, draftMinRank } = this.state
    this.props.updateSettings({
      price: draftPrice,
      minRank: draftMinRank,
    })
  }
  toggleModal() {
    const { isModalOpen } = this.state
    const { minRank, price } = this.props
    this.setState({
      isModalOpen: !isModalOpen,
      draftMinRank: minRank,
      draftPrice: price,
    })
  }
  render() {
    const {
      wip, instructions, command, toggle, readOnly, isEnabled, minRank, price, noSettings,
    } = this.props

    const toggleClassNames = classNames({
      'Command-toggle': true,
      'Command-toggle-disabled': !isEnabled,
      'Command-toggle-wip': wip,
    });
    const stateLabel = wip ? 'coming soon' : isEnabled ? 'ENABLED' : 'DISABLED'
    const rankLabel = {
      [RANKS.EVERYONE]: '',
      [RANKS.FOLLOWER]: 'Followers & Subs only',
      [RANKS.SUBSCRIBER]: 'Subs only',
      [RANKS.MOD]: 'Mods only',
      [RANKS.BROADCASTER]: 'Broadcaster only',
    }[minRank]

    return (
      <div>
        <div className="Widgets-instructions">
          {instructions}. {rankLabel && <span className="Command-instructions-rank">{rankLabel}.</span>} {!!price && <span className="Command-instructions-price">Costs {price} coins per use.</span>}
        </div>
        <div className="Command-commandContainer">
          <div className="Command-command">{command}</div>
          {!readOnly && (
            <div onClick={wip ? null : toggle} className={toggleClassNames}>{stateLabel}</div>
          )}
          {!readOnly && !wip && (
            <div className="Command-settingsContainer">
              {!noSettings && (
                <div
                  tabIndex={0}
                  role="button"
                  className="Command-settings"
                  onClick={this.toggleModal}
                >
                  <i className="fa fa-cogs" aria-hidden="true" />
                </div>
              )}
              <Modal open={this.state.isModalOpen} className="Command-modal">
                <Modal.Content>
                  <h1 className="Command-modal-title">{command}</h1>
                  <small className="Command-modal-instructions">{instructions}</small>
                  <div className="Command-modal-field">
                    <h2>How many coins does this command cost per use?</h2>
                    <input
                      type="number"
                      value={this.state.draftPrice}
                      onChange={this.onChangePrice}
                    />
                  </div>
                  <div className="Command-modal-field">
                    <h2>Who can use this command?</h2>
                    <select value={this.state.draftMinRank} onChange={this.onChangeMinRank}>
                      <option value="EVERYONE">Everyone</option>
                      <option value="FOLLOWER">Followers and Subscribers</option>
                      <option value="SUBSCRIBER">Subscribers only</option>
                      <option value="MOD">Mods only</option>
                      <option value="BROADCASTER">Broadcaster only</option>
                    </select>
                  </div>
                </Modal.Content>
                <Modal.Actions>
                  <Button onClick={this.toggleModal} negative>Cancel</Button>
                  <Button
                    onClick={this.save}
                    positive
                    icon="checkmark"
                    labelPosition="right"
                    content="Save"
                  />
                </Modal.Actions>
              </Modal>
            </div>
          )}
        </div>
      </div>
    )
  }
}

Command.defaultProps = {
  wip: false,
  readOnly: false,
  minRank: 'EVERYONE',
  price: 0,
  updateSettings: () => {},
  noSettings: false,
  toggle: () => {},
  isEnabled: true,
}

Command.propTypes = {
  wip: PropTypes.bool,
  instructions: PropTypes.string.isRequired,
  command: PropTypes.string.isRequired,
  toggle: PropTypes.func,
  updateSettings: PropTypes.func,
  readOnly: PropTypes.bool,
  isEnabled: PropTypes.bool,
  minRank: PropTypes.string,
  price: PropTypes.number,
  noSettings: PropTypes.bool,
}

export default Command
