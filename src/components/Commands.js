import React, { Component } from 'react'
import { Grid } from 'react-flexbox-grid'
import { firebaseClient } from '../lib/redux-saga-firebase'
import defaultCommands from '../lib/default-commands'
import Command from './Command'
import ChatBotSettings from './ChatBotSettings'

class Commands extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      userSettings: null,
    }
  }
  componentWillMount() {
    const username = this.props.match.params.username

    firebaseClient.database().ref(`usernameToIdMap/${username.toLowerCase()}`).once('value', (snapshot) => {
      const userId = snapshot.val()

      if (userId) {
        firebaseClient.database().ref(`userSettings/${userId}`).on('value', (snapshot2) => {
          const userSettings = snapshot2.val()
          this.setState({
            userSettings,
            isLoading: false,
          })
        })
      }
    })
  }
  getCommandConfig(key) {
    if (!this.state.userSettings[key]) {
      return false
    }
    return this.state.userSettings[key]
  }
  getCommandState(key) {
    return !!(this.state.userSettings[key] && this.state.userSettings[key].enabled)
  }
  render() {
    const channelName = this.props.match.params.username
    if (this.state.isLoading) {
      return null
    }
    const customCommands = this.state.userSettings.custom
      ? Object.keys(this.state.userSettings.custom).map(key => this.state.userSettings.custom[key])
      : []
    const enabledCommands = defaultCommands.filter((category) => {
      const enabled = category.commands
        .filter(command => this.getCommandConfig(command.key).enabled)
      return enabled.length > 0
    })

    return (
      <Grid className="Widgets">
        <h1 className="Widgets-title">{channelName.toUpperCase()} CHAT COMMANDS</h1>
        {customCommands.length > 0 && (
          <div>
            <h1 className="Widgets-title">CUSTOM</h1>
            {customCommands.map(command => (
              <Command
                readOnly
                key={command.trigger}
                instructions={`Response -> ${command.response}`}
                isEnabled={command.enabled}
                command={command.trigger}
                noSettings
              />
            ))}
          </div>
        )}
        {enabledCommands.map((category) => {
          const commands = category.commands.map((command) => {
            const config = this.getCommandConfig(command.key)
            const { price, minRank } = config
            return this.getCommandState(command.key) && (
              <Command
                readOnly
                key={command.key}
                minRank={minRank}
                price={price}
                instructions={command.instructions}
                command={command.command}
              />
            )
          })
          const categorySettings = category.key && this.getCommandConfig(category.key)
          return (
            <div key={category.title}>
              <h1 className="Widgets-title">{category.title}</h1>
              {categorySettings && (
                <ChatBotSettings category={category} config={categorySettings} />
              )}
              {commands}
            </div>
          )
        })}
      </Grid>
    )
  }
}

export default Commands
