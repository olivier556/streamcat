import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Grid } from 'react-flexbox-grid'
import Iframe from 'react-iframe'

import { getViewersChart } from '../ducks/channel'

class Dashboard extends Component {
  render() {
    const { title, gameName, username } = this.props
    return (
      <Grid className="Widgets">
        <div>Stream title: {title}</div>
        <div>Stream game: {gameName}</div>
        <div>Stream preview:</div>
        <Iframe
          url={`https://twitch.tv/${username}/embed`}
          width="600px"
          height="360px"
        />
      </Grid>
    )
  }
}

export default connect(state => ({
  username: state.auth.username,
  viewersChart: getViewersChart(state),
  title: state.channel.title,
  gameName: state.channel.gameName,
}))(Dashboard)
