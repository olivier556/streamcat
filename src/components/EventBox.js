import React from 'react'
import { connect } from 'react-redux'
import TimeAgo from 'react-timeago'
import FlipMove from 'react-flip-move'

import { eventsSelector } from '../ducks/events'
import './EventBox.css'

const EventBox = ({ events }) => (
  <div>
    {events.length > 0 ? (
      <FlipMove duration={500} easing="ease-in-out" enterAnimation="accordionVertical" appearAnimation="fade">
        {events.map(event => {
          if (event.type === 'FOLLOWER') {
            return (
              <div className="Event-card" key={'FOLLOWER-' + event.fromUserId}>
                <h1 className="Event-text"><span className="Event-username">{event.username}</span> just followed you</h1>
                <TimeAgo className="Event-time" date={event.timestamp} />
              </div>
            );
          } else if (event.type === 'SUBSCRIBER') {
            const planName = {
              1000: '4.99',
              2000: '9.99',
              3000: '24.99',
            }[event.plan] || event.plan
            return (
              <div className="Event-card" key={'SUBSCRIBER-' + event.fromUserId}>
                <h1 className="Event-text"><span className="Event-username">{event.username}</span> just subscribed with a <span className="Event-username">{planName}</span> sub for {event.months} {event.months > 1 ? 'months' : 'month'}</h1>
                {event.message && (
                  <h2 className="Event-message">{event.message}</h2>
                )}
                <TimeAgo className="Event-time" date={event.timestamp} />
              </div>
            )
          } else if (event.type === 'CHEER') {
            return (
              <div className="Event-card" key={'CHEER-' + event.fromUserId}>
                <h1 className="Event-text"><span className="Event-username">{event.username}</span> just cheered <span className="Event-username">{event.bits}</span> bits</h1>
                {event.message && (
                  <h2 className="Event-message">{event.message}</h2>
                )}
                <TimeAgo className="Event-time" date={event.timestamp} />
              </div>
            )
          }
          return null
        })}
      </FlipMove>
    ) : (
      <div className="Dashboard-empty">
        Starting now, all new followers, subscribers, cheers and bitcoin donationn will show up here.
      </div>
    )}
  </div>
)

export default connect(state => ({
  events: eventsSelector(state),
}))(EventBox)
