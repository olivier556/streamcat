import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import { logout } from '../ducks/auth'
import './Header.css'

class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
    }
    this.toggleMenu = this.toggleMenu.bind(this)
  }
  toggleMenu() {
    const { isOpen } = this.state
    this.setState({
      isOpen: !isOpen,
    })
  }
  render() {
    const { authenticated, username, logout } = this.props
    const { isOpen } = this.state

    if (authenticated) {
      return (
        <div className="Header">
          <div role="button" className="Header-userButton" onClick={this.toggleMenu}>
            {username}
            <i className="Header-userButton-icon fa fa-caret-down" aria-hidden="true" />
          </div>
          {isOpen && (
            <div onClick={this.toggleMenu} className="Header-menuContainer">
              <Link className="Header-nav" to="/">Home</Link>
              <Link className="Header-nav" to="/dashboard">Dashboard</Link>
              <Link className="Header-nav" to="/widgets">Widgets</Link>
              <Link className="Header-nav" to="/commands">Commands</Link>
              <div className="Header-nav-separator"></div>
              <div className="Header-nav Header-nav-small" onClick={logout}>Logout</div>
            </div>
          )}
        </div>
      )
    } else {
      return null
    }
  }
}

export default connect(state => ({
  authenticated: state.auth.authenticated,
  username: state.auth.username,
}), dispatch => ({
  logout: () => dispatch(logout()),
}))(Header);
