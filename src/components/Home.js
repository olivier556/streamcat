import { Grid, Row, Col } from 'react-flexbox-grid'
import React from 'react'
import config from '../lib/config'
import Cat from './Cat'
import './App.css'

const features = [{
  name: 'LIVE DASHBOARD',
  bullets: [
    'Be notified when your channel gets a new follower, subscriber, or cheer.',
    'Quickly use and monitor Twitch chat via the embedded pane.',
    'Easily enable or disable features and set permissions.',
  ],
}, {
  name: 'COMMANDS',
  bullets: [
    'Includes all of the commands you expect from a Twitch bot!',
    'Access mokeybot exclusive commands such as <span class="Features-command">!speedrun</span> and <span class="Features-command">!wolfram</span>.',
    'Share your enabled commands on a public page via <span class="Features-command">!commands</span>.',
    'Create even more custom commands.',
  ],
}, {
  name: 'WIDGETS',
  bullets: [
    'Mokeybot widgets apply code updates and user preferences automatically without hassle.',
    'Notifications widget.',
    'Bitcoin Donation widget.',
  ],
}, {
  name: 'SONG REQUESTS',
  bullets: [
    'Request songs with <span class="Features-command">!sr song name, YouTube link, video id, or search query</span>.',
    'Share your playlist on a live public page via <span class="Features-command">!playlist</span>.',
    'Enable community engagement commands like <span class="Features-command">!skip</span> and <span class="Features-command">!save</span>.',
    'Fully control permissions for each command.',
  ],
}, {
  name: 'CURRENCY',
  bullets: [
    'Viewers earn coins by watching your stream.',
    'Moderate command use with customisable fees.',
    'Reward viewers with coins via <span class="Features-command">!give</span>.',
    'Enable <span class="Features-command">!tip</span> to encourage positive chat behaviour.',
  ],
}]

const Home = () => (
  <Grid className="App">
    <Cat />
    <h1 className="App-title">mokeybot</h1>
    <p className="App-text">
      Hello Twitch streamer, I'm mokeybot, a computer program made to make your life a little bit easier with things such as notification overlays, dashboards & a chatbot. Yep, all the good stuff. All in one place. <br /><a href="https://discord.gg/Hx6weQF">Join my Discord server</a> for updates.
    </p>
    <a
      className="App-twitch"
      href={`https://api.twitch.tv/kraken/oauth2/authorize?response_type=${config.twitchResponseType}&client_id=${config.twitchClientId}&redirect_uri=${config.twitchRedirectUri}&scope=${config.twitchScope}&force_verify=true`}>
      <i className="App-TwitchLogo fa fa-twitch fa-lg" />CONNECT WITH TWITCH
    </a>
    <div className="Home-features">
      <h2 className="Home-features-title">FEATURES</h2>
      <ul className="Home-features-list">
        {features.map(feature => (
          <li key={feature.name} className="Home-feature">
            <h3 className="Home-feature-title">{feature.name}</h3>
            <ul className="Home-feature-bullets">
              {feature.bullets.map(bullet => (
                <li
                  key={bullet}
                  className="Home-feature-bullet"
                  dangerouslySetInnerHTML={{ __html: bullet }}
                />
              ))}
            </ul>
          </li>
        ))}
      </ul>
    </div>
  </Grid>
)

export default Home
