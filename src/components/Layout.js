import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-flexbox-grid'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import moment from 'moment'
import Iframe from 'react-iframe'

import Header from './Header'
import Footer from './SongPlayer'
import EventBox from './EventBox'
import './Layout.css'

class Layout extends Component {
  render() {
    const { authenticated, username, children, liveSince, isLive, viewerCount } = this.props

    const now = moment(new Date())
    const uptimeMoment = moment(+liveSince)
    const hours = now.diff(uptimeMoment, 'hours')
    uptimeMoment.add(hours, 'hours')
    const minutes = now.diff(uptimeMoment, 'minutes')
    uptimeMoment.add(minutes, 'minutes')
    const seconds = now.diff(uptimeMoment, 'seconds')

    let formatted
    if (hours && minutes && seconds) {
      formatted = `${hours}h ${minutes}m`
    } else if (minutes && seconds) {
      formatted = `${minutes}m`
    } else if (seconds) {
      formatted = 'less than a minute'
    } else {
      console.log('WTF')
    }

    return (
      <Grid fluid className="Layout">
        {authenticated ? (
          <Row>
            <Col sm={0} md={4} lg={3} className="LeftPanel">
              <div className="LeftPanel-top">
                <EventBox />
              </div>
              <div className="LeftPanel-info">
                {isLive ? (
                  <div>
                    <i className="fa fa-circle LeftPanel-LiveCircle" aria-hidden="true" />
                    Live - {viewerCount} viewers - Live for {formatted}
                  </div>
                ) : (
                  <div>
                    <i className="fa fa-circle LeftPanel-OfflineCircle" aria-hidden="true" />
                    Stream is currently offline
                  </div>
                )}
              </div>
              <Iframe
                className="Sidebar"
                frameborder="100%"
                scrolling="yes"
                url={`https://www.twitch.tv/${username}/chat?darkpopout`}
                height="70%"
                theme="dark"
                width="100%" />
            </Col>
            <Col sm={12} md={8} lg={9}>
              <Header />
              {children}
              <Footer />
            </Col>
          </Row>
        ) : (
          <div>
            <Header />
              {children}
            <Footer />
          </div>
        )}
      </Grid>
    )
  }
}

export default withRouter(connect(state => ({
  authenticated: state.auth.authenticated,
  username: state.auth.username,
  viewerCount: state.channel.viewerCount,
  isLive: state.channel.isLive,
  liveSince: state.channel.liveSince,
}))(Layout))

// export default Layout
