import React, { Component } from 'react'
import { Grid } from 'react-flexbox-grid'
import Moment from 'react-moment'

import { firebaseClient } from '../lib/redux-saga-firebase'
import './Leaderboard.css'

class Leaderboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      viewers: []
    }
  }
  componentWillMount() {
    const username = this.props.match.params.username

    firebaseClient.database().ref(`usernameToIdMap/${username.toLowerCase()}`).on('value', (snapshot) => {
      const userId = snapshot.val()
      if (userId) {
        firebaseClient.database()
          .ref(`channels/${userId}/viewers`)
          .orderByChild('points')
          .limitToLast(100)
          .on('value', snapshot => {
            const viewersRaw = snapshot.val()
            const viewers = Object.keys(viewersRaw).map(key => ({...viewersRaw[key], username: key})).sort((a, b) => b.points - a.points)
            this.setState({
              viewers,
              isLoading: false
            })
          })
      }
    })
  }
  toggleCommand(key) {
    const newState = !this.getCommandState(key)
    this.props.updateUserSettings({[key]: newState})
  }
  getCommandState(key) {
    return !!this.state.userSettings[key]
  }
  render() {
    const channelName = this.props.match.params.username
    if (this.state.isLoading) {
      return null
    }
    return (
      <Grid className="Widgets">
        <h1 className="Widgets-title">{channelName.toUpperCase()} LEADERBOARD</h1>
        {this.state.viewers.map((viewer, idx) => {
          return (
            <div className="Leaderboard-card" key={viewer.username}>
              {idx + 1}. {viewer.username} ({viewer.points} coins) Last seen <Moment fromNow>{viewer.lastSeen}</Moment>
            </div>
          )
        })}
      </Grid>
    )
  }
}

export default Leaderboard
