import React, { Component } from 'react'
import { Grid } from 'react-flexbox-grid'
import Moment from 'react-moment'
import { firebaseClient } from '../lib/redux-saga-firebase'
import './Playlist.css'

class Commands extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      playlist: null
    }
  }
  componentWillMount() {
    const username = this.props.match.params.username

    firebaseClient.database().ref(`usernameToIdMap/${username.toLowerCase()}`).once('value', (snapshot) => {
      const userId = snapshot.val()

      if (userId) {
        firebaseClient.database().ref(`playlist/${userId}`).on('value' , snapshot => {
          const rawPlaylist = snapshot.val() || {}
          const playlist = Object.keys(rawPlaylist).map(id => ({
            id,
            ...rawPlaylist[id]
          }))
          this.setState({
            playlist,
            isLoading: false
          })
        })
      }
    })
  }
  render() {
    const channelName = this.props.match.params.username
    if (this.state.isLoading) {
      return null
    }
    return (
      <Grid className="Widgets">
        <h1 className="Widgets-title">{channelName.toUpperCase()}'S PLAYLIST</h1>
        {this.state.playlist.length > 0 ? (
          <div>
            {this.state.playlist.map(song => {
              return (
                <div className="Playlist-songCard" key={song.id}>
                  <div className="Playlist-songTitle">{song.title} ({Math.round(song.durationInSeconds / 60)}m)</div>
                  <div className="Playlist-songDetails">requested by <span className="Playlist-songDetails-bold">{song.requestedByUsername}</span> <Moment fromNow>{song.timestamp}</Moment></div>
                </div>
              )
            })}
          </div>
        ) : (
          <h1 className="Playlist-noSongs">There are no songs. Use `!sr song name` to add songs to the playlist.</h1>
        )}
      </Grid>
    )
  }
}

export default Commands
