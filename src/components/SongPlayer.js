import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Grid, Row, Col } from 'react-flexbox-grid'
import './SongPlayer.css'
import { updateUserSettings } from '../ducks/userSettings'
import classNames from 'classnames'
import YouTube from 'react-youtube'
import { nextSong, play, pause } from '../ducks/playlist'
import { Link } from 'react-router-dom'

class ChatBot extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isHidden: true
    }
  }
  toggleHidden() {
    const isHidden = this.state.isHidden
    this.setState({
      isHidden: !isHidden
    })
  }
  render() {
    if (!this.props.authenticated) return null
    const { isAutoplaying, currentSong, songsLeft } = this.props
    const { isHidden } = this.state

    const opts = {
      height: '236px',
      width: '420px',
      playerVars: {
        autoplay: isAutoplaying ? 1 : 0,
        controls: 1,
        // showinfo: 0,
        modestbranding: 1
      }
    }
    const youtubeClassNames = classNames({
      'SongPlayer-youtube': true,
      'SongPlayer-youtube-hidden': isHidden
    })
    return (
      <div className="SongPlayer">
        {currentSong ? (
          <div>
            <div className="SongPlayer-controls">
              <div onClick={this.props.nextSong} className="SongPlayer-control SongPlayer-control-skip">
                <i className="SongPlayer-control-icon fa fa-step-forward" aria-hidden="true"></i>
                SKIP SONG
              </div>
              <Link to={`/playlist/${this.props.username}`}>
                <div className="SongPlayer-control">
                  <i className="SongPlayer-control-icon fa fa-list" aria-hidden="true"></i>
                  {songsLeft} {songsLeft === 1 ? 'song' : 'songs' } on queue
                </div>
              </Link>
              <div onClick={this.toggleHidden.bind(this)} className="SongPlayer-control">
                {isHidden ? (
                  <i className="fa fa-eye SongPlayer-control-icon" aria-hidden="true"></i>
                ) : (
                  <i className="fa fa-eye-slash SongPlayer-control-icon" aria-hidden="true"></i>
                )}
                {isHidden ? 'SHOW VIDEO' : 'HIDE VIDEO'}
              </div>
            </div>
            <YouTube
              className={youtubeClassNames}
              opts={opts}
              onEnd={this.props.nextSong}
              onPause={this.props.pause}
              onPlay={this.props.play}
              videoId={currentSong.youtubeId} />
          </div>
        ) : (
          <div className="SongPlayer-playlistInfo">No songs on queue. To add songs use !sr on chat. Make sure the !sr command is enabled on the chatbot page.</div>
        )}
      </div>
    )
  }
}

export default connect(state => ({
  currentSong: state.playlist.currentSong,
  authenticated: state.auth.authenticated,
  songsLeft: state.playlist.songQueue.length,
  isAutoplaying: state.playlist.isAutoplaying,
  username: state.auth.username,
}), dispatch => ({
  nextSong: () => dispatch(nextSong()),
  play: () => dispatch(play()),
  pause: () => dispatch(pause()),
}))(ChatBot)
