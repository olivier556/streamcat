import React, { Component } from 'react';
import config from '../lib/config';
import { connect } from 'react-redux';
import Iframe from 'react-iframe'
import './Widgets.css'
import { Grid, Row, Col } from 'react-flexbox-grid';
import { updateUserSettings } from '../ducks/userSettings';
import { testEvent } from '../ducks/events';
import Notifications, { success, error } from 'react-notification-system-redux';

class NotificationWidget extends Component {
  constructor(props) {
    super(props)
    this.state = {
      bitcoinAddressDraft: ''
    }
  }
  updateBitcoinAddress() {
    const bitcoinAddressDraft = this.state.bitcoinAddressDraft
    if (isValidAddress(bitcoinAddressDraft)) {
      this.props.pushNotification('Success!', 'Your Bitcoin address has been updated')
      if (bitcoinAddressDraft) {
        this.props.updateUserSettings({
          bitcoinAddress: bitcoinAddressDraft
        })
      }
    } else {
      this.props.pushErrorNotification('Error!', 'That doesn\'t look like a Bitcoin address')
    }

    function isValidAddress(address) {
      return address.length > 25 && address.length < 36 && (address[0] === '1' || address[0] === '3')
    }
  }
  render() {
    const simpleNotificationsUrl = `${config.staticUrl}/simple-notifications?channelId=${this.props.userId}`
    const previewSimpleNotificationsUrl = simpleNotificationsUrl + '&preview=true'
    const bitcoinDonationUrl = `${config.staticUrl}/bitcoin-donation?channelId=${this.props.userId}`
    const previewBitcoinDonationUrl = bitcoinDonationUrl + '&preview=true'
    return (
      <Grid className="Widgets">
        <Notifications
          notifications={this.props.notifications}
        />
        <div>
          <h1 className="Widgets-title">NOTIFICATIONS WIDGET</h1>
          <div className="Widgets-instructions">Use this URL as a browser source on OBS or XSplit</div>
          <div className="Widgets-copyPasta">{simpleNotificationsUrl}</div>
          <div className="Widgets-instructions">Test notifications (this will active notifications on both the preview and the widget itself!)</div>
          <div className="Widgets-buttonGroup">
            <div className="Widgets-button" onClick={() => this.props.testEvent('SUBSCRIBER')}>Subscription</div>
            <div className="Widgets-button" onClick={() => this.props.testEvent('FOLLOWER')}>Follower</div>
            <div className="Widgets-button" onClick={() => this.props.testEvent('CHEER')}>Cheer</div>
          </div>
          <div className="Widgets-instructions">Preview</div>
          <Iframe url={previewSimpleNotificationsUrl}
            width="450px"
            height="450px"
            id="myId"
            className="Widgets-iframe"
            display="initial"
            position="relative"
            allowFullScreen />
        </div>
        <div>
          <h1 className="Widgets-title">BITCOIN DONATION WIDGET</h1>
          <div className="Widgets-instructions">Submit your bitcoin address</div>
          <div className="Widgets-textInputContainer">
            <input
              value={this.state.bitcoinAddressDraft || this.props.bitcoinAddress}
              onChange={e => this.setState({bitcoinAddressDraft: e.target.value})}
              className="Widgets-textInput" type="text"/>
            <button
              onClick={this.updateBitcoinAddress.bind(this)}
              className="Widgets-textInputButton">
              update
            </button>
          </div>
          <div className="Widgets-instructions">Use this URL as a browser source on OBS or XSplit</div>
          <div className="Widgets-copyPasta">{bitcoinDonationUrl}</div>
          <div className="Widgets-instructions">Preview</div>
          <Iframe url={previewBitcoinDonationUrl}
            width="450px"
            height="450px"
            id="myId"
            className="Widgets-iframe"
            display="initial"
            position="relative"
            allowFullScreen />
        </div>
      </Grid>
    );
  }
}

export default connect(state => ({
  userId: state.auth.userId,
  bitcoinAddress: state.userSettings.bitcoinAddress,
  notifications: state.notifications
}), dispatch => ({
  updateUserSettings: (userSettings) => dispatch(updateUserSettings(userSettings)),
  pushNotification: (title, message) => dispatch(success({title, message})),
  pushErrorNotification: (title, message) => dispatch(error({title, message})),
  testEvent: (type) => dispatch(testEvent(type))
}))(NotificationWidget);
