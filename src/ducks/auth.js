import config from '../lib/config';

export const LOGIN = 'sc/auth/LOGIN';
export const LOGOUT = 'sc/auth/LOGOUT';
export const AUTHENTICATED = 'sc/auth/AUTHENTICATED';
const UPDATE_USER = 'sc/auth/UPDATE_USER';

const defaultState = {
  username: localStorage.getItem(config.authUsernameKey) || '',
  authenticated: !!localStorage.getItem(config.authUserIdKey),
  userId: localStorage.getItem(config.authUserIdKey),
}

export default function reducer(state = defaultState, action = {}) {
  switch (action.type) {
    case UPDATE_USER:
      return {
        ...state,
        username: action.username,
        userId: action.userId,
        authenticated: true
      }
    case AUTHENTICATED:
      if (action.authenticated) {
        return {...state, authenticated: true}
      } else {
        return {authenticated: false}
      }
    default: return state;
  }
}

export function authenticated(state) {
  return {
    type: AUTHENTICATED,
    authenticated: state
  }
}

export function login(twitchCode) {
  return {
    type: LOGIN,
    twitchCode
  };
}

export function logout() {
  return {
    type: LOGOUT
  }
}

export function updateUser(username, userId) {
  return {
    type: UPDATE_USER,
    username,
    userId
  }
}
