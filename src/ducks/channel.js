export const SYNC_CHANNEL = 'sc/channel/SYNC_CHANNEL'

const defaultState = {
  isLive: false,
  viewerCount: 0,
  liveSince: Date.now(),
  title: '',
  gameName: '',
  ticks: [],
}

export default function reducer(state = defaultState, action = {}) {
  switch (action.type) {
    case SYNC_CHANNEL:
      return {
        ...state,
        ...action.channel,
      }
    default: return state
  }
}

export function syncChannel(channel) {
  return {
    type: SYNC_CHANNEL,
    channel,
  }
}

export function getViewersChart(state) {
  return state.channel.ticks
}
