export const SYNC_FOLLOWERS = 'sc/events/SYNC_FOLLOWERS';
export const SYNC_SUBSCRIBERS = 'sc/events/SYNC_SUBSCRIBERS';
export const SYNC_CHEERS = 'sc/events/SYNC_CHEERS';
export const TEST_EVENT = 'sc/events/TEST_EVENT';0

const defaultState = {
  followers: [],
  subscribers: [],
  cheers: []
}

export default function reducer(state = defaultState, action = {}) {
  switch (action.type) {
    case SYNC_FOLLOWERS:
      return {...state, followers: action.followers}
    case SYNC_SUBSCRIBERS:
      return {...state, subscribers: action.subscribers}
    case SYNC_CHEERS:
      return {...state, cheers: action.cheers}
    default: return state;
  }
}

export function syncFollowers(followers) {
  return {
    type: SYNC_FOLLOWERS,
    followers
  }
}

export function testEvent(type) {
  return {
    type: TEST_EVENT,
    eventType: type
  }
}

export function syncSubscribers(subscribers) {
  return {
    type: SYNC_SUBSCRIBERS,
    subscribers
  }
}

export function syncCheers(cheers) {
  return {
    type: SYNC_CHEERS,
    cheers
  }
}
export function eventsSelector(state) {
  const followersObject = state.events.followers
  const cheersObject = state.events.cheers
  const subscribersObject = state.events.subscribers

  const followersArray = Object.keys(followersObject)
    .map(id => ({...followersObject[id], type: 'FOLLOWER'}))
  const cheersArray = Object.keys(cheersObject)
    .map(id => ({...cheersObject[id], type: 'CHEER'}))
  const subscribersArray = Object.keys(subscribersObject)
    .map(id => ({...subscribersObject[id], type: 'SUBSCRIBER'}))

  return [].concat.apply([], [followersArray, cheersArray, subscribersArray]).sort((a, b) => b.timestamp - a.timestamp)
}
