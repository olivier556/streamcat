import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { reducer as notifications } from 'react-notification-system-redux'

import auth from './auth'
import events from './events'
import userSettings from './userSettings'
import playlist from './playlist'
import channel from './channel'

const reducers = {
  auth,
  events,
  userSettings,
  notifications,
  playlist,
  channel,
  routing: routerReducer,
};

const rootReducer = combineReducers(reducers)
export default rootReducer
