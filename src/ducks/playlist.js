export const SYNC_PLAYLIST = 'sc/playlist/SYNC_PLAYLIST'
export const NEXT_SONG = 'sc/playlist/NEXT_SONG'
export const SET_AUTOPLAY = 'sc/playlist/SET_AUTOPLAY'

const defaultState = {
  currentSong: null,
  songQueue: [],
  isAutoplaying: false,

}

export default function reducer(state = defaultState, action = {}) {
  switch (action.type) {
    case SYNC_PLAYLIST:
      if (state.currentSong) {
        const currentSongHasBeenSkipped = !action.playlist.find(song => song.id === state.currentSong.id)
        return {
          ...state,
          songQueue: action.playlist,
          currentSong: currentSongHasBeenSkipped ? action.playlist[0] : state.currentSong
        }
      } else {
        return {
          ...state,
          songQueue: action.playlist,
          currentSong: action.playlist[0]
        }
      }
    case SET_AUTOPLAY:
      return{
        ...state,
        isAutoplaying: action.autoplay,
      }
    case NEXT_SONG:
      return {
        ...state
      }
    default: return state;
  }
}

export function nextSong() {
  return {
    type: NEXT_SONG
  }
}

export function play() {
  return {
    type: SET_AUTOPLAY,
    autoplay: true
  }
}

export function pause() {
  return {
    type: SET_AUTOPLAY,
    autplay: false
  }
}

export function syncPlaylist(rawPlaylist) {
  const playlist = Object.keys(rawPlaylist).map(id => ({
    id,
    ...rawPlaylist[id]
  }))
  return {
    type: SYNC_PLAYLIST,
    playlist
  }
}
