export const SYNC_USER_SETTINGS = 'sc/events/SYNC_USER_SETTINGS';
export const UPDATE_USER_SETTINGS = 'sc/events/UPDATE_USER_SETTINGS';

const defaultState = {
  bitcoinAddress: ''
}

export default function reducer(state = defaultState, action = {}) {
  switch (action.type) {
    case SYNC_USER_SETTINGS:
      return {...defaultState, ...action.userSettings}
    default: return state;
  }
}

export function syncUserSettings(userSettings) {
  return {
    type: SYNC_USER_SETTINGS,
    userSettings
  }
}

export function updateUserSettings(userSettings) {
  return {
    type: UPDATE_USER_SETTINGS,
    userSettings
  }
}
