import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Route, Redirect, Switch } from 'react-router-dom'
import { createBrowserHistory } from 'history';
import { routerMiddleware, ConnectedRouter } from 'react-router-redux';

import registerServiceWorker from './lib/registerServiceWorker'
import reducer from './ducks/index'
import saga from './sagas/index'
import Layout from './components/Layout'
import Auth from './components/Auth'
import Home from './components/Home'
import Widgets from './components/Widgets'
import ChatBot from './components/ChatBot'
import Commands from './components/Commands'
import Playlist from './components/Playlist'
import Leaderboard from './components/Leaderboard'
import Dashboard from './components/Dashboard'
import './index.css'

const sagaMiddleware = createSagaMiddleware();
const browserHistory = createBrowserHistory();

let store = createStore(reducer, composeWithDevTools(applyMiddleware(routerMiddleware(browserHistory), sagaMiddleware)));

sagaMiddleware.run(saga);

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    store.getState().auth.authenticated ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/',
        state: { from: props.location }
      }}/>
    )
  )}/>
)

render(
  <Provider store={store}>
    <ConnectedRouter history={browserHistory}>
      <Layout>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/auth" component={Auth} />
          <Route path="/commands/:username" component={Commands} />
          <Route path="/playlist/:username" component={Playlist} />
          <Route path="/leaderboard/:username" component={Leaderboard} />
          <PrivateRoute path="/commands" component={ChatBot} />
          <PrivateRoute path="/widgets" component={Widgets} />
          <PrivateRoute path="/dashboard" component={Dashboard} />
          <Redirect to="/" />
        </Switch>
      </Layout>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();
