import config from './config';

export function login(twitchCode) {
  debugger
  return fetch(config.apiUrl + '/login?twitchCode=' + twitchCode)
    .then(res => res.json())
    .then(json => json)
    .catch(error => console.log('ERROR', error))
}

export function getUserDetails(token) {
  return fetch('https://api.twitch.tv/helix/users', {
    headers: {
      'Authorization': 'Bearer ' + token,
      'Client-ID': config.twitchClientId
    }
  })
    .then(res => res.json())
    .then(json => console.log(json))
}
