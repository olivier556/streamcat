const dev = {
  twitchClientId: 'sjzrx4xgrtss7pt76hyx0mp0cfihhh',
  twitchClientSecret: 'quhyzfcjh1rktevvzlgajyuwrcn28i',
  twitchScope: 'channel_subscriptions channel_check_subscription',
  twitchRedirectUri: 'http://localhost:3000/auth',
  twitchResponseType: 'code',
  twitchPubSubUrl: 'wws://pubsub-edge.twitch.tv',

  firebaseApiKey: 'AIzaSyDp5IqiPTZ5-QAIjowUN9s5hfRLl-biUFY',
  firebaseAuthDomain: 'mokeybot-dev.firebaseapp.com',
  firebaseDatabaseURL: 'https://mokeybot-dev.firebaseio.com',
  firebaseProjectId: 'mokeybot-dev',
  firebaseMessagingSenderId: '1065670085737',
  firebaseAdminKeyName: 'firebase-dev',

  authUserIdKey: 'mokeybot-userid',
  authUsernameKey: 'mokeybot-username',

  devServerPort: 4002,
  apiUrl: 'http://localhost:4002',
  staticUrl: 'http://localhost:4002',
  twitchWebhookUrl: 'http://73.96.100.212:4002',
  clientUrl: 'localhost:3000',

  botUserId: '185232142',
  botUsername: 'mokeybot',

  ytKey: 'AIzaSyA6fcMMQfmjgnluwHYZkuSrmT1R234WjYE',
}

const prod = {
  ...dev,
  twitchClientId: 'g5m3kdqbl04vrn9zzjzoh5vxlo2249',
  twitchClientSecret: 'f4xu0iz18pxmaol2fifdadhhce63au',
  twitchRedirectUri: 'https://mokeybot.com/auth',

  firebaseApiKey: 'AIzaSyDoI-2SaxKI6ol9I7DuCic2hoo-vo0btoc',
  firebaseAuthDomain: 'streamcat-120be.firebaseapp.com',
  firebaseDatabaseURL: 'https://streamcat-120be.firebaseio.com',
  firebaseProjectId: 'streamcat-120be',
  firebaseMessagingSenderId: '1019864536597',
  firebaseAdminKeyName: 'firebase-prod',

  apiUrl: 'https://api.mokeybot.com',
  staticUrl: 'https://widget.mokeybot.com',
  twitchWebhookUrl: 'https://api.mokeybot.com',
  clientUrl: 'mokeybot.com',
}

const isProduction = process.env.NODE_ENV === 'production'

if (isProduction) {
  module.exports = prod
} else {
  module.exports = dev
}
