export const RANKS = {
  EVERYONE: 'EVERYONE',
  FOLLOWER: 'FOLLOWER',
  SUBSCRIBER: 'SUBSCRIBER',
  MOD: 'MOD',
  BROADCASTER: 'BROADCASTER',
}

const defaultCommands = [
  {
    title: 'INFORMATIONAL',
    commands: [
      {
        key: 'commands',
        instructions: 'Get a list of available commands',
        command: '!commands',
        defaultConfig: {
          minRank: RANKS.EVERYONE,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'uptime',
        instructions: 'Find out how long the stream has been live for',
        command: '!uptime',
        defaultConfig: {
          minRank: RANKS.EVERYONE,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'seen',
        instructions: 'Find out when the target user was last seen on this stream',
        command: '!seen [target username]',
        defaultConfig: {
          minRank: RANKS.EVERYONE,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'speedrun',
        instructions: 'Find out world record speedrun times',
        command: '!speedrun [target game] [target category]',
        defaultConfig: {
          minRank: RANKS.EVERYONE,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'followage',
        instructions: 'Find out how long you have been following this channel for',
        command: '!followage',
        defaultConfig: {
          minRank: RANKS.EVERYONE,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'watched',
        instructions: 'Find out how much time you have spent on this stream',
        command: '!watched',
        defaultConfig: {
          minRank: RANKS.EVERYONE,
          price: 0,
          enabled: true,
        },
      },
    ],
  },
  {
    title: 'MUSIC',
    key: 'music',
    defaultConfig: {
      maxSongLengthMinutes: 7,
      voteCountToSkip: 3,
    },
    commands: [
      {
        key: 'sr',
        instructions: 'Song request',
        command: '!sr [song name OR youtube video id]',
        defaultConfig: {
          minRank: RANKS.FOLLOWER,
          price: 50,
          enabled: true,
        },
      },
      {
        key: 'playlist',
        instructions: 'Get a list of songs added via !sr',
        command: '!playlist',
        defaultConfig: {
          minRank: RANKS.FOLLOWER,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'currentsong',
        instructions: 'Get information about the song that is currently playing',
        command: '!currentsong',
        defaultConfig: {
          minRank: RANKS.FOLLOWER,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'nextsong',
        instructions: 'Get information about the upcoming song',
        command: '!nextsong',
        defaultConfig: {
          minRank: RANKS.FOLLOWER,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'skippay',
        instructions: 'Pay to instantly skip the current song',
        command: '!skippay',
        defaultConfig: {
          minRank: RANKS.FOLLOWER,
          price: 100,
          enabled: true,
        },
      },
      {
        key: 'skip',
        instructions: 'Vote to skip the song currently playing',
        command: '!skip',
        defaultConfig: {
          minRank: RANKS.FOLLOWER,
          price: 0,
          enabled: true,
        },
      },
    ],
  },
  {
    title: 'FUN',
    commands: [
      {
        key: 'wolfram',
        instructions: 'Ask Wolfram Alpha anything',
        command: '!wolfram [query]',
        defaultConfig: {
          minRank: RANKS.EVERYONE,
          price: 20,
          enabled: true,
        },
      },
      {
        key: '8ball',
        instructions: 'Gives a random answer to any yes/no question',
        command: '!8ball [question]',
        defaultConfig: {
          minRank: RANKS.EVERYONE,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'fliptable',
        instructions: 'Flip a darn table (╯°□°)╯︵ ┻━┻',
        command: '!fliptable',
        defaultConfig: {
          minRank: RANKS.EVERYONE,
          price: 0,
          enabled: true,
        },
      },
    ],
  },
  {
    title: 'CURRENCY',
    key: 'currency',
    defaultConfig: {
      coinsEarnedPerMinute: 1,
    },
    commands: [
      {
        key: 'coins',
        instructions: 'Find out how many coins you have (coins are earned every minute that you are present on a stream)',
        command: '!coins OR !coins [username]',
        defaultConfig: {
          minRank: RANKS.EVERYONE,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'tip',
        instructions: 'Transfer coins from your account to another user\'s account',
        command: '!tip [username] [amount]',
        defaultConfig: {
          minRank: RANKS.EVERYONE,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'leaderboard',
        instructions: 'Find out who the top coin holders are',
        command: '!leaderboard',
        defaultConfig: {
          minRank: RANKS.EVERYONE,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'give',
        instructions: 'Give coins to a user',
        command: '!give [username] [amount]',
        defaultConfig: {
          minRank: RANKS.BROADCASTER,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'subtract',
        instructions: 'Take coins away from a user',
        command: '!subtract [username] [amount]',
        defaultConfig: {
          minRank: RANKS.BROADCASTER,
          price: 0,
          enabled: true,
        },
      },
    ],
  },
  {
    title: 'CREATE COMMANDS',
    commands: [
      {
        key: 'commandadd',
        instructions: 'Add a custom command',
        command: '!command add [command] [response]',
        defaultConfig: {
          minRank: RANKS.MOD,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'commandedit',
        instructions: 'Edit a custom command',
        command: '!command edit [command] [response]',
        defaultConfig: {
          minRank: RANKS.MOD,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'commanddelete',
        instructions: 'Delete a custom command',
        command: '!command delete [command]',
        defaultConfig: {
          minRank: RANKS.MOD,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'commanddisable',
        instructions: 'Disable a custom command',
        command: '!command disable [command]',
        defaultConfig: {
          minRank: RANKS.MOD,
          price: 0,
          enabled: true,
        },
      },
      {
        key: 'commandenable',
        instructions: 'Enable a custom command',
        command: '!command enable [command]',
        defaultConfig: {
          minRank: RANKS.MOD,
          price: 0,
          enabled: true,
        },
      },
    ],
  },
]

export function getDefaultConfig() {
  const out = {}
  defaultCommands
    .forEach((category) => {
      if (category.key) {
        out[category.key] = category.defaultConfig
      }
      category.commands.forEach((command) => {
        out[command.key] = command.defaultConfig
      })
    })
  return out
}

export default defaultCommands
