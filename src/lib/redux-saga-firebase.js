import firebase from 'firebase';
import ReduxSagaFirebase from 'redux-saga-firebase';
import config from './config';

const myFirebaseApp = firebase.initializeApp({
  apiKey: config.firebaseApiKey,
  authDomain: config.firebaseAuthDomain,
  databaseURL: config.firebaseDatabaseURL,
});

const reduxSagaFirebase = new ReduxSagaFirebase(myFirebaseApp);

window.firebase = myFirebaseApp
myFirebaseApp.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
export const firebaseClient = myFirebaseApp
export default reduxSagaFirebase;
