import { takeLatest, takeEvery, call, put, fork, take, cancel, select } from 'redux-saga/effects'
import deepExtend from 'deep-extend'
import { push } from 'react-router-redux'
import { channel } from 'redux-saga'

import { LOGOUT, updateUser, authenticated } from '../ducks/auth'
import { syncChannel } from '../ducks/channel'
import { syncFollowers, syncCheers, syncSubscribers, TEST_EVENT } from '../ducks/events'
import { syncUserSettings, UPDATE_USER_SETTINGS } from '../ducks/userSettings'
import { NEXT_SONG, syncPlaylist } from '../ducks/playlist'
import * as api from '../lib/api'
import rsf, { firebaseClient } from '../lib/redux-saga-firebase'
import config from '../lib/config'
import { getDefaultConfig } from '../lib/default-commands'

const authChangeChannel = channel()

function* loginSaga({ twitchCode }) {
  try {
    const { firebaseToken, username, userId } = yield call(api.login, twitchCode)
    yield call(rsf.auth.signInWithCustomToken, firebaseToken)
    window.localStorage.setItem(config.authUsernameKey, username)
    window.localStorage.setItem(config.authUserIdKey, userId)
    yield put(updateUser(username, userId))
    yield put(push('/commands'))
  } catch (error) {
    yield put(push('/'))
  }
}

function* logoutSaga() {
  yield call(rsf.auth.signOut)
  window.localStorage.removeItem(config.authUsernameKey)
  window.localStorage.removeItem(config.authUserIdKey)
  yield put(push('/'))
}

function* syncFollowersSaga() {
  const userId = yield select(state => state.auth.userId)
  // const userId = 17337557

  const ref = firebaseClient.database()
    .ref(`channels/${userId}/followers`)
    .orderByChild('timestamp')
    .startAt(1)
    .limitToLast(25)
  const channel2 = yield call(rsf.database.channel, ref)

  while (true) {
    const data = yield take(channel2)
    const { value } = data
    if (value) {
      yield put(syncFollowers(value))
    }
  }
}

function* syncSubscribersSaga() {
  const userId = yield select(state => state.auth.userId)
  // const userId = 17337557

  const ref = firebaseClient.database()
    .ref(`channels/${userId}/subscribers`)
    .orderByChild('timestamp')
    .startAt(1)
    .limitToLast(25)
  const channel2 = yield call(rsf.database.channel, ref);

  while (true) {
    const data = yield take(channel2);
    const { value } = data
    if (value) {
      yield put(syncSubscribers(value));
    }
  }
}

function* updateUserSettingsSaga(userSettingsPartial) {
  const userSettingsAll = yield select(state => state.userSettings)
  const userSettings = deepExtend(userSettingsAll, userSettingsPartial.userSettings)
  const userId = yield select(state => state.auth.userId)
  firebaseClient.database()
    .ref(`userSettings/${userId}`)
    .set(userSettings)
}

function* userSettingsSaga() {
  const userId = yield select(state => state.auth.userId)

  const ref = firebaseClient.database()
    .ref(`userSettings/${userId}`)

  const channel2 = yield call(rsf.database.channel, ref);
  let firstTime = true

  while (true) {
    const data = yield take(channel2)
    const { value } = data
    if (value) {
      if (firstTime) {
        const defaultConfig = getDefaultConfig()
        const finalUserSettings = deepExtend(defaultConfig, value || {})
        yield updateUserSettingsSaga({ userSettings: finalUserSettings })
        yield put(syncUserSettings(finalUserSettings))
        firstTime = false
      } else {
        yield put(syncUserSettings(value))
      }
    } else {
      // possibly first login
      const defaultConfig = getDefaultConfig()
      yield updateUserSettingsSaga({ userSettings: defaultConfig })
      yield put(syncUserSettings(defaultConfig))
      firstTime = false
    }
  }
}
function* syncPlaylistSaga() {
  const userId = yield select(state => state.auth.userId)
  const ref = firebaseClient.database()
    .ref(`playlist/${userId}`)
    .orderByChild('timestamp')
  const channel2 = yield call(rsf.database.channel, ref)
  while (true) {
    const data = yield take(channel2)
    const value = data.value || []
    if (value) {
      yield put(syncPlaylist(value))
    }
  }
}
function* syncChannelSaga() {
  const userId = yield select(state => state.auth.userId)

  const ref = firebaseClient.database()
    .ref(`channels/${userId}/meta`)
    .orderByKey()
    .limitToLast(1)
  const channel2 = yield call(rsf.database.channel, ref)

  while (true) {
    const data = yield take(channel2)
    const { value } = data
    if (value) {
      const startedAt = Object.keys(value)[0]
      const rawChannel = Object.keys(value[startedAt]).map(id => value[startedAt][id]).sort((a, b) => b.timestamp - a.timestamp)
      const latestTick = rawChannel[0]
      const data = {
        isLive: Date.now() - latestTick.timestamp < (1000 * 120),
        viewerCount: latestTick.viewerCount,
        title: latestTick.title,
        gameName: latestTick.gameName,
        liveSince: startedAt,
        ticks: rawChannel,
      }
      yield put(syncChannel(data))
    }
  }
}
function* syncCheersSaga() {
  const userId = yield select(state => state.auth.userId)

  const ref = firebaseClient.database()
    .ref(`channels/${userId}/cheers`)
    .orderByChild('timestamp')
    .startAt(1)
    .limitToLast(25)
  const channel2 = yield call(rsf.database.channel, ref);

  while (true) {
    const data = yield take(channel2);
    const value = data.value;
    if (value) {
      yield put(syncCheers(value));
    }
  }
}
function* authenticationSaga() {
  let tasks = [];

  while(true) {
    const action = yield take(authChangeChannel);
    yield put(action);

    for (let i = 0; i < tasks.length; i++) {
      const task = tasks[i];
      yield cancel(task);
    }

    if (action.authenticated) {
      tasks.push(yield fork(syncFollowersSaga));
      tasks.push(yield fork(syncSubscribersSaga));
      tasks.push(yield fork(syncCheersSaga));
      tasks.push(yield fork(userSettingsSaga));
      tasks.push(yield fork(syncPlaylistSaga));
      tasks.push(yield fork(syncChannelSaga));
    }
  }
}
function* nextSongSaga() {
  const userId = yield select(state => state.auth.userId)
  const currentSong = yield select(state => state.playlist.currentSong)
  yield call(rsf.database.delete, `playlist/${userId}/${currentSong.id}`)
}
function* testEventSaga({ eventType }) {
  const userId = yield select(state => state.auth.userId)
  const fromUserId = 'TEST' + getRandomInt(1000, 10000000000);

  const usernames = ['Starboard_', 'Arnude', 'howlmyname', 'drowzkh', 'Loli_Susan', 'JimmyHere', 'milezo1', 'Lucidtea', 'MONTYvsTHEWORLD', 'rabidmanatee', 'xXxMcCallXx', 'Monst3r_x', 'crystalship024', 'katebots', 'triggerfingerdante']
  const randomUsername = usernames[Math.floor(Math.random()*usernames.length)];
  if (eventType === 'FOLLOWER') {
    yield call(rsf.database.update, `channels/${userId}/followers/${fromUserId}`, {
      fromUserId,
      timestamp: 0,
      testTimestamp: Date.now(),
      username: randomUsername,
      viewers: 100,
      profileImageUrl: 'https://static-cdn.jtvnw.net/user-default-pictures/cd618d3e-f14d-4960-b7cf-094231b04735-profile_image-300x300.jpg',
      isTest: true,
    });
  } else if (eventType === 'SUBSCRIBER') {
    yield call(rsf.database.update, `channels/${userId}/subscribers/${fromUserId}`, {
      // message,
      // emotes: userstate.emotes,
      username: randomUsername,
      timestamp: 0,
      testTimestamp: Date.now(),
      plan: 'PRIME',
      months: 1,
      fromUserId,
    })
  } else if (eventType === 'CHEER') {
    yield call(rsf.database.update, `channels/${userId}/cheers/${fromUserId}`, {
      // message,
      // emotes: userstate.emotes,
      username: randomUsername,
      testTimestamp: Date.now(),
      timestamp: 0,
      bits: getRandomInt(100, 3000),
      fromUserId,
    })
  }
}

firebaseClient.auth().onAuthStateChanged((user) => {
  if (!user) {
    authChangeChannel.put(authenticated(false))
  } else {
    authChangeChannel.put(authenticated(true))
  }
})

function* authRouteSaga(route) {
  if (route.payload.pathname === '/auth') {
    const query = route.payload.search
    const params = new URLSearchParams(query)
    const accessToken = params.get('code')
    yield loginSaga({ twitchCode: accessToken })
  }
}

export default function* saga() {
  yield [
    takeLatest('@@router/LOCATION_CHANGE', authRouteSaga),
    takeLatest(LOGOUT, logoutSaga),
    takeLatest(UPDATE_USER_SETTINGS, updateUserSettingsSaga),
    takeEvery(TEST_EVENT, testEventSaga),
    takeLatest(NEXT_SONG, nextSongSaga),
  ]

  yield fork(authenticationSaga)
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}
